
QT       += core gui opengl

QMAKE_CXXFLAGS = -Wno-unused-parameter -Wno-parentheses

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WorldEditor
TEMPLATE = app


INCLUDEPATH += /usr/include/towerengine /usr/include/libxml2 /usr/local/include/bullet

LIBS += -L/usr/local/lib -ltowerengine -lGL -lGLU -lxml2 -lBulletCollision -lBulletDynamics -lLinearMath

SOURCES +=  src/main.cpp\
	    src/mainwindow.cpp \
	    src/renderwidget.cpp \
	    src/worldeditor.cpp \
	    src/object/editobject.cpp \
	    src/editaxisobject.cpp \
	    src/gridobject.cpp \
	    src/sceneproject.cpp \
	    src/editwidget/vectoreditwidget.cpp \
	    src/editwidget/coloreditwidget.cpp \
	    src/editwidget/floateditwidget.cpp \
	    src/editwidget/stringeditwidget.cpp \
	    src/selection.cpp \
	    src/editwidget/asseteditwidget.cpp \
	    src/selectassetdialog.cpp \
	    src/editwidget/fileeditwidget.cpp \
	    src/asset/editasset.cpp \
	    src/editscene.cpp \
	    src/towerstyle.cpp \
	    src/edittransform.cpp \
	    src/editwidget/transformeditwidget.cpp \
	    src/property.cpp \
	    src/object/mesheditobject.cpp \
	    src/object/directionallighteditobject.cpp \
	    src/object/pointlighteditobject.cpp \
	    src/asset/mesheditasset.cpp \
	    src/asset/cubemapeditasset.cpp \
	    src/asset/singlefileeditasset.cpp \
	    src/xmlutils.cpp \
	    src/viewsettingsdialog.cpp \
  	    src/sceneproject_save.cpp \
            src/sceneproject_export.cpp \
            src/sceneproject_load.cpp \
    src/editspace.cpp \
    src/object/editobjectspaceconnection.cpp

INCLUDEPATH += include include/editwidget include/object include/asset

HEADERS  += include/mainwindow.h \
	    include/renderwidget.h \
	    include/worldeditor.h \
	    include/object/editobject.h \
	    include/editaxisobject.h \
	    include/gridobject.h \
	    include/sceneproject.h \
	    include/outlineitemdata.h \
	    include/editwidget/vectoreditwidget.h \
	    include/editwidget/floateditwidget.h \
	    include/editwidget/stringeditwidget.h \
	    include/asset/editasset.h \
	    include/selection.h \
	    include/selectassetdialog.h \
	    include/editscene.h \
	    include/towerstyle.h \
	    include/edittransform.h \
	    include/editwidget/transformeditwidget.h \
	    include/property.h \
	    include/object/mesheditobject.h \
	    include/object/directionallighteditobject.h \
	    include/object/pointlighteditobject.h \
	    include/asset/mesheditasset.h \
	    include/asset/cubemapeditasset.h \
	    include/asset/singlefileeditasset.h \
	    include/xmlutils.h \
	    include/viewsettingsdialog.h \
            include/editwidget/asseteditwidget.h \
            include/editwidget/coloreditwidget.h \
            include/editwidget/fileeditwidget.h \
    include/allincludes.h \
    include/editspace.h \
    include/object/editobjectspaceconnection.h

FORMS    += ui/mainwindow.ui \
	    ui/vectoreditwidget.ui \
	    ui/coloreditwidget.ui \
	    ui/floateditwidget.ui \
	    ui/stringeditwidget.ui \
	    ui/asseteditwidget.ui \
	    ui/selectassetdialog.ui \
	    ui/fileeditwidget.ui \
	    ui/transformeditwidget.ui \
	    ui/viewsettingsdialog.ui

RESOURCES += resources.qrc
