#include "allincludes.h"

#include <QFile>

CEditAsset::CEditAsset(CSceneProject *project, string name)
{
	this->project = project;
	SetName(name);

	AddProperty(new CEditAssetNameProperty(this));
}

void CEditAsset::SetName(string name)
{
	this->name = project->GetUniqueAssetName(name, this);
	project->AssetNameChanged(this);
}

xmlNodePtr CEditAsset::CreateSceneProjectXMLNode(void)
{
	xmlNodePtr n = xmlNewNode(0, GetSceneProjectXMLNodeName());
	xmlNodePtr child;

	xmlSetProp(n, tcxc("name"), tcxc(GetName().c_str()));

	for(int i=0; i<GetPropertiesCount(); i++)
	{
		child = GetProperty(i)->CreateSceneProjectXMLNode();
		if(!child)
			continue;
		xmlAddChild(n, child);
	}

	return n;
}

xmlNodePtr CEditAsset::CreateSceneExportXMLNode(void)
{
	xmlNodePtr n = xmlNewNode(0, GetSceneExportXMLNodeName());
	xmlNodePtr child;

	xmlSetProp(n, tcxc("name"), tcxc(GetName().c_str()));

	for(int i=0; i<GetPropertiesCount(); i++)
	{
		child = GetProperty(i)->CreateSceneExportXMLNode();
		if(!child)
			continue;
		xmlAddChild(n, child);
	}

	return n;
}



CEditAssetNameProperty::CEditAssetNameProperty(CEditAsset *asset)
{
	this->asset = asset;
}

QWidget *CEditAssetNameProperty::CreateEditWidget(CMainWindow *main_win)
{
	CStringEditWidget *w = new CStringEditWidget(QString("Name"), asset->GetName(), main_win);
	w->SetCallback(this);
	return w;
}

void CEditAssetNameProperty::ValueChanged(string s, string *c)
{
	if(asset->GetName() == s)
		return;

	asset->SetName(s);
	*c = asset->GetName();
}

