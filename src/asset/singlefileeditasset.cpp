
#include "allincludes.h"

#include <QFile>



CSingleFileEditAsset::CSingleFileEditAsset(CSceneProject *project, string name, bool export_base64) : CEditAsset(project, name)
{
	filename = string();
	data = QByteArray();
	this->export_base64 = export_base64;

	AddProperty(new CSingleFileEditAssetFilenameProperty(this));
}

CSingleFileEditAsset::CSingleFileEditAsset(CSceneProject *project, string name, string filename, bool export_base64) : CEditAsset(project, name)
{
	this->filename = filename;
	this->export_base64 = export_base64;

	LoadFile();
}

CSingleFileEditAsset::~CSingleFileEditAsset(void)
{
}

void CSingleFileEditAsset::LoadFile(void)
{
	data = QByteArray();

	if(filename.size() > 0)
	{
		QFile f(filename.data());
		f.open(QIODevice::ReadOnly);
		if(f.isOpen())
			data = f.readAll();
	}

	FileChanged();
}

void CSingleFileEditAsset::SetFileName(string f)
{
	if(filename == f)
		return;

	filename = f;
	LoadFile();
}

string CSingleFileEditAsset::GetFileExtension(void)
{
	size_t d = filename.find_last_of('.');
	string ext = string("");

	if(d != string::npos)
		ext = filename.substr(d);

	return ext;
}


CSingleFileEditAssetFilenameProperty::CSingleFileEditAssetFilenameProperty(CSingleFileEditAsset *asset)
{
	this->asset = asset;
}

QWidget *CSingleFileEditAssetFilenameProperty::CreateEditWidget(CMainWindow *main_win)
{
	CFileEditWidget *w = new CFileEditWidget(QString("File"), asset->GetFileName(), asset->GetFileFilter(), main_win);
	w->SetCallback(this);
	return w;
}

xmlNodePtr CSingleFileEditAssetFilenameProperty::CreateSceneProjectXMLNode(void)
{
	xmlNodePtr n = xmlNewNode(0, (const xmlChar *)"filename");
	xmlSetProp(n, tcxc("v"), tcxc(asset->GetFileName().c_str()));
	return n;
}

xmlNodePtr CSingleFileEditAssetFilenameProperty::CreateSceneExportXMLNode(void)
{
	xmlNodePtr n = xmlNewNode(0, (const xmlChar *)"data");
	xmlSetProp(n, tcxc("filename_ext"), (const xmlChar *)asset->GetFileExtension().data());

	const char *export_data;
	QByteArray base64;

	if(asset->GetExportBase64())
	{
		base64 = export_data = asset->GetData().toBase64();
		export_data = base64.constData();
	}
	else
		export_data = asset->GetData().constData();

	xmlNodeSetContent(n, (const xmlChar *)export_data);
	return n;
}

void CSingleFileEditAssetFilenameProperty::ValueChanged(string v)
{
	asset->SetFileName(v);
}
