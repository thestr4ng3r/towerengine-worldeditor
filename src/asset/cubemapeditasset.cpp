#include "allincludes.h"

#include <QFile>

CCubeMapEditAsset::CCubeMapEditAsset(CSceneProject *project, string name, string filename) : CSingleFileEditAsset(project, name, true)
{
	cube_map = 0;

	SetFileName(filename);
}

CCubeMapEditAsset::~CCubeMapEditAsset(void)
{
	if(cube_map)
		glDeleteTextures(1, &cube_map);
}

void CCubeMapEditAsset::FileChanged(void)
{
	if(cube_map)
		glDeleteTextures(1, &cube_map);

	QByteArray data = GetData();

	cube_map = 0;

	if(data.size() > 0)
	{
		string ext = GetFileExtension();
		cube_map = LoadGLCubeMapBinary(ext.data(), data.data(), data.length());
	}
}
