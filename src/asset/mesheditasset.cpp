#include "allincludes.h"

#include <QFile>

CMeshEditAsset::CMeshEditAsset(CSceneProject *project, string name, string filename) : CSingleFileEditAsset(project, name, false)
{
	mesh = 0;
	SetFileName(filename);
}

CMeshEditAsset::~CMeshEditAsset(void)
{
	if(mesh)
		delete mesh;
}


void CMeshEditAsset::FileChanged(void)
{
	if(mesh)
		delete mesh;

	mesh = 0;

	QByteArray data = GetData();

	if(data.size() > 0)
	{
		mesh = new tMesh();
		if(!mesh->LoadFromData(data.data()))
		{
			delete mesh;
			mesh = 0;
		}
	}
}
