
#include "allincludes.h"

bool CSceneProject::LoadFromFile(string file)
{
	xmlDocPtr doc = xmlReadFile(file.data(), 0, XML_PARSE_HUGE);

	if(!doc)
		return false;

	xmlNodePtr cur;

	cur = xmlDocGetRootElement(doc);
	if(!cur)
		return false;

	if(!xmlStrEqual(cur->name, (const xmlChar *)"tsceneproject"))
		return false;



	cur = cur->children;

	while(cur)
	{
		if(xmlStrEqual(cur->name, (const xmlChar *)"assets"))
			ParseAssetsNode(cur);
		else if(xmlStrEqual(cur->name, (const xmlChar *)"objects"))
			ParseObjectsNode(cur);
		else if(xmlStrEqual(cur->name, (const xmlChar *)"edit_camera"))
			ParseEditCameraNode(cur);
		else if(xmlStrEqual(cur->name, (const xmlChar *)"scene"))
			ParseSceneNode(cur);

		cur = cur->next;
	}

	this->filename = file;

	return true;
}


void CSceneProject::ParseAssetsNode(xmlNodePtr cur)
{
	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, (const xmlChar *)"mesh"))
			ParseMeshAssetNode(child);
		else if(xmlStrEqual(child->name, (const xmlChar *)"cubemap"))
			ParseCubeMapAssetNode(child);
	}
}

void CSceneProject::ParseMeshAssetNode(xmlNodePtr cur)
{
	xmlChar *temp;

	string name;
	string filename;

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"name")))
		return;
	name = string((const char *)temp);

	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, tcxc("filename")))
		{
			if(!(temp = xmlGetProp(child, tcxc("v"))))
				continue;
			filename = string(tcc(temp));
		}
	}

	CMeshEditAsset *mesh = new CMeshEditAsset(this, name, filename);
	AddAsset(mesh);
}

void CSceneProject::ParseCubeMapAssetNode(xmlNodePtr cur)
{
	xmlChar *temp;

	string name;
	string filename;

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"name")))
		return;
	name = string((const char *)temp);

	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, tcxc("filename")))
		{
			if(!(temp = xmlGetProp(child, tcxc("v"))))
				continue;
			filename = string(tcc(temp));
		}
	}

	CCubeMapEditAsset *cubemap = new CCubeMapEditAsset(this, name, filename);
	AddAsset(cubemap);
}

void CSceneProject::ParseObjectsNode(xmlNodePtr cur)
{
	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, (const xmlChar *)"mesh"))
			ParseMeshObjectNode(child);
		else if(xmlStrEqual(child->name, (const xmlChar *)"dir_light"))
			ParseDirectionalLightObjectNode(child);
		else if(xmlStrEqual(child->name, (const xmlChar *)"point_light"))
			ParsePointLightObjectNode(child);
	}
}

void CSceneProject::ParseMeshObjectNode(xmlNodePtr cur)
{
	xmlChar *temp;
	string name;
	string asset_name;
	CEditTransform transform;

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"name")))
		return;
	name = string((const char *)temp);

	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, tcxc("mesh_asset")))
		{
			if(!(temp = xmlGetProp(child, tcxc("asset"))))
				continue;
			asset_name = string(tcc(temp));
		}
		else if(xmlStrEqual(child->name, tcxc("transform")))
			transform = ParseEditTransformXMLNode(child);
	}

	CEditAsset *asset = FindAssetByName(asset_name);

	if(asset)
		if(asset->GetType() != EDIT_ASSET_TYPE_MESH)
			return;

	CMeshEditObject *object = new CMeshEditObject(this, (CMeshEditAsset *)asset);
	object->SetName(name);
	object->SetTransform(transform);
	AddObject(object);
}

void CSceneProject::ParseDirectionalLightObjectNode(xmlNodePtr cur)
{
	xmlChar *temp;
	string name;
	tVector direction = Vec(0.0, 0.0, 0.0);
	tVector color = Vec(0.0, 0.0, 0.0);

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"name")))
		return;
	name = string((const char *)temp);

	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, tcxc("direction")))
			direction = ParseVectorXMLNode(child);
		else if(xmlStrEqual(child->name, tcxc("color")))
			color = ParseVectorXMLNode(child, tcxc("r"), tcxc("g"), tcxc("b"));
	}

	CDirectionalLightEditObject *object = new CDirectionalLightEditObject(this, direction, color);
	object->SetName(name);
	AddObject(object);
}

void CSceneProject::ParsePointLightObjectNode(xmlNodePtr cur)
{
	xmlChar *temp;
	string name;
	tVector position = Vec(0.0, 0.0, 0.0);
	tVector color = Vec(0.0, 0.0, 0.0);
	float distance = 0.0;

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"name")))
		return;
	name = string((const char *)temp);

	for(xmlNodePtr child = cur->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, tcxc("position")))
			position = ParseVectorXMLNode(child);
		else if(xmlStrEqual(child->name, tcxc("color")))
			color = ParseVectorXMLNode(child, tcxc("r"), tcxc("g"), tcxc("b"));
		else if(xmlStrEqual(child->name, tcxc("distance")))
			distance = ParseFloatXMLNode(child);
	}

	CPointLightEditObject *object = new CPointLightEditObject(this, position, color, distance);
	object->SetName(name);
	AddObject(object);
}


void CSceneProject::ParseEditCameraNode(xmlNodePtr cur)
{
	tVector pos;
	tVector2 rot;
	xmlChar *temp;

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"x")))
		return;
	pos.x = atof((const char *)temp);
	if(!(temp = xmlGetProp(cur, (const xmlChar *)"y")))
		return;
	pos.y = atof((const char *)temp);
	if(!(temp = xmlGetProp(cur, (const xmlChar *)"z")))
		return;
	pos.z = atof((const char *)temp);

	if(!(temp = xmlGetProp(cur, (const xmlChar *)"rx")))
		return;
	rot.x = atof((const char *)temp);
	if(!(temp = xmlGetProp(cur, (const xmlChar *)"ry")))
		return;
	rot.y = atof((const char *)temp);

	world_editor->GetEditSpace()->SetEditCamera(pos, rot);
}

void CSceneProject::ParseSceneNode(xmlNodePtr cur)
{
	xmlChar *temp;

	if(temp = xmlGetProp(cur, (const xmlChar *)"sky_cubemap"))
	{
		CEditAsset *asset = FindAssetByName(string((const char *)temp));
		if(asset->GetType() == EDIT_ASSET_TYPE_CUBE_MAP)
			scene->SetSkyCubeMap((CCubeMapEditAsset *)asset);
	}
}
