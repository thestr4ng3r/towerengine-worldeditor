#include "towerstyle.h"

#include <QtGui>
#include <QStyleOption>

CTowerStyle::CTowerStyle(QStyle *parent) : QProxyStyle(parent)
{
}

QPainterPath CTowerStyle::roundRectPath(const QRect &rect)
{
	int radius = 8;
	int diam = 2 * radius;

	int x1, y1, x2, y2;
	rect.getCoords(&x1, &y1, &x2, &y2);

	QPainterPath path;
	path.moveTo(x2, y1 + radius);
	path.arcTo(QRect(x2 - diam, y1, diam, diam), 0.0, +90.0);
	path.lineTo(x1 + radius, y1);
	path.arcTo(QRect(x1, y1, diam, diam), 90.0, +90.0);
	path.lineTo(x1, y2 - radius);
	path.arcTo(QRect(x1, y2 - diam, diam, diam), 180.0, +90.0);
	path.lineTo(x1 + radius, y2);
	path.arcTo(QRect(x2 - diam, y2 - diam, diam, diam), 270.0, +90.0);
	path.closeSubpath();
	return path;
}

int CTowerStyle::pixelMetric(PixelMetric metric, const QStyleOption *option, const QWidget *widget) const
{
	switch(metric)
	{
		default:
			return QProxyStyle::pixelMetric(metric, option, widget);
	}
}

void CTowerStyle::drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
{
	QPen pen;

	pen.setColor(QColor(QColor(255, 255, 0)));
	pen.setWidth(2);
	pen.setJoinStyle(Qt::RoundJoin);

	switch(element)
	{
		case PE_PanelButtonCommand:
			//break;
		default:
			QProxyStyle::drawPrimitive(element, option, painter, widget);
			break;
	}
}
