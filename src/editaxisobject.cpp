#include "allincludes.h"

const int move_axis_vertex_count = 25;
const float move_axis_vertex[move_axis_vertex_count * 3] = { 0.014142081141471863, -0.014142089523375034, 0.800000011920929, 0.019999954849481583, 4.140459708423805e-08, 0.800000011920929, 0.09999997168779373, 5.602717578767624e-08, 0.7999999523162842, 0.07071060687303543, -0.07071059942245483, 0.7999999523162842, -4.962086563864432e-08, 0.020000023767352104, -1.5099598238421663e-09, -4.962086563864432e-08, 0.020000046119093895, 0.800000011920929, 0.014142081141471863, 0.014142175205051899, 0.800000011920929, 0.014142081141471863, 0.014142150059342384, -1.0677027040273401e-09, -5.136932301752495e-08, -0.01999996416270733, 0.800000011920929, -5.847490314181414e-08, -0.09999996423721313, 0.7999999523162842, 0.019999954849481583, 1.7245271877186497e-08, -1.3064227485053278e-15, -0.014142181724309921, -0.014142089523375034, 0.800000011920929, -0.07071071118116379, -0.07071059942245483, 0.7999999523162842, 0.014142081141471863, -0.014142114669084549, 1.0676999284697786e-09, -0.020000044256448746, 4.251732477200676e-08, 0.800000011920929, -0.10000001639127731, 6.159080356837876e-08, 0.7999999523162842, -5.136932301752495e-08, -0.01999998651444912, 1.5099570482846048e-09, -0.014142181724309921, 0.014142175205051899, 0.800000011920929, -0.07071071118116379, 0.07071071863174438, 0.7999999523162842, -0.014142181724309921, -0.014142114669084549, 1.0676999284697786e-09, -4.9732626905552024e-08, 0.10000008344650269, 0.7999999523162842, 0.07071060687303543, 0.07071071863174438, 0.7999999523162842, -0.020000044256448746, 1.8357997788598368e-08, -1.3815493828499823e-15, -0.014142181724309921, 0.014142150059342384, -1.0677027040273401e-09, -7.450580596923828e-08, 7.549789415861596e-08, 0.9999999403953552 };
const int move_axis_index_count = 138;
const int move_axis_index[move_axis_index_count] = { 0, 1, 2, 2, 3, 0, 4, 5, 6, 6, 7, 4, 8, 0, 3, 3, 9, 8, 7, 6, 1, 1, 10, 7, 11, 8, 9, 9, 12, 11, 10, 1, 0, 0, 13, 10, 14, 11, 12, 12, 15, 14, 13, 0, 8, 8, 16, 13, 17, 14, 15, 15, 18, 17, 16, 8, 11, 11, 19, 16, 20, 21, 6, 6, 5, 20, 19, 11, 14, 14, 22, 19, 5, 17, 18, 18, 20, 5, 23, 17, 5, 5, 4, 23, 22, 14, 17, 17, 23, 22, 21, 2, 1, 1, 6, 21, 4, 7, 23, 7, 10, 13, 13, 16, 19, 19, 22, 23, 7, 13, 23, 13, 19, 23, 12, 9, 24, 15, 12, 24, 3, 2, 24, 2, 21, 24, 21, 20, 24, 9, 3, 24, 18, 15, 24, 20, 18, 24 };


CEditAxisObject::CEditAxisObject(CEditSpace *space, tVector color, tVector dir) : tObject()
{
	this->space = space;
	pos = Vec(0.0, 0.0, 0.0);
	this->dir = dir;
	this->color = color;
	visible = true;

	motion_state = new btDefaultMotionState();
	shape = new btBoxShape(btVector3(0.1, 0.1, 0.5));
	rigid_body = new btRigidBody(0.0, motion_state, shape);
	rigid_body->setUserPointer(this);
	space->GetPhysicsWorld()->addRigidBody(rigid_body);

	vao = new tVAO();
	vbo = new tVBO<float>(3, vao, move_axis_vertex_count);
	UpdateVBO();

	ibo = new tIBO(vao, move_axis_index_count);
	memcpy(ibo->GetData(), move_axis_index, move_axis_index_count * sizeof(int));
	ibo->AssignData();
}

CEditAxisObject::~CEditAxisObject(void)
{
	space->GetPhysicsWorld()->removeRigidBody(rigid_body);

	delete rigid_body;
	delete shape;
	delete motion_state;
}

void CEditAxisObject::UpdateVBO(void)
{
	tVector x, y, z;
	tVector a, b;
	float size = dir.Len();

	z = dir;
	z.Normalize();

	if(Dot(z, Vec(0.0, 1.0, 0.0)) > 0.9)
		x = Cross(Vec(0.0, 0.0, 1.0), z);
	else
		x = Cross(Vec(0.0, 1.0, 0.0), z);

	y = Cross(z, x);

	x.Normalize();
	y.Normalize();

	x *= size;
	y *= size;
	z *= size;

	for(int i=0; i<move_axis_vertex_count; i++)
	{
		a = Vec(move_axis_vertex[i*3], move_axis_vertex[i*3+1], move_axis_vertex[i*3+2]);
		b = x * a.x + y * a.y + z * a.z;
		b += pos;
		vbo->GetData()[i*3] = b.x;
		vbo->GetData()[i*3+1] = b.y;
		vbo->GetData()[i*3+2] = b.z;
	}

	vbo->AssignData();


	btTransform trans;
	trans.setOrigin(BtVec(pos + z * 0.5));
	trans.setBasis(btMatrix3x3(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z));

	rigid_body->setWorldTransform(trans);
}

void CEditAxisObject::ForwardPass(void)
{
	if(!visible)
		return;


	glDisable(GL_DEPTH_TEST);

	tEngine::GetColorShader()->Bind();
	tEngine::GetColorShader()->SetTransformation(tEngine::identity_matrix4);
	tEngine::GetColorShader()->SetDiffuseColor(color);

	vbo->SetAttribute(tFaceShader::vertex_attribute, GL_FLOAT);

	ibo->Draw(GL_TRIANGLES);
}

tBoundingBox CEditAxisObject::GetBoundingBox(void)
{
	tBoundingBox b;
	b.SetBounds(pos, pos + dir);
	return b;
}


void CEditAxisObject::AddedToWorld(tWorld *world)
{
}

void CEditAxisObject::RemovedFromWorld(tWorld *world)
{
}
