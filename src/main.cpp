#include "allincludes.h"
#include "towerstyle.h"

#include "renderwidget.h"


int main(int argc, char *argv[])
{
	QPalette p;

	p.setColor(QPalette::Window, QColor(100, 100, 100));
	p.setColor(QPalette::WindowText, QColor(200, 200, 200));
	p.setColor(QPalette::Base, QColor(150, 150, 150));
	p.setColor(QPalette::AlternateBase, QColor(170, 170, 170));
	p.setColor(QPalette::ToolTipBase, QColor(200, 200, 200));
	p.setColor(QPalette::ToolTipText, QColor(50, 50, 50));
	p.setColor(QPalette::Text, QColor(0, 0, 0));
	p.setColor(QPalette::Button, QColor(50, 50, 50));
	p.setColor(QPalette::ButtonText, QColor(200, 200, 200));
	p.setColor(QPalette::BrightText, QColor(255, 255, 255));

	p.setColor(QPalette::Highlight, QColor(50, 50, 50));
	p.setColor(QPalette::HighlightedText, QColor(200, 200, 200));

	//QApplication::setPalette(p);
	//QApplication::setStyle(new CTowerStyle);
	QApplication a(argc, argv);

	setlocale(LC_NUMERIC, "C");

	QGLFormat glf = QGLFormat::defaultFormat();
	glf.setSampleBuffers(true);
	glf.setSamples(4);
	QGLFormat::setDefaultFormat(glf);

	CWorldEditor *world_editor = new CWorldEditor();

	if(a.arguments().size() > 1)
		world_editor->LoadSceneProject(a.arguments().at(1).toStdString());

	return a.exec();
}
