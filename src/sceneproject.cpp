
#include "allincludes.h"


CSceneProject::CSceneProject(CWorldEditor *editor)
{
	world_editor = editor;
	loaded = true;
	InitEmptyScene();
}

CSceneProject::CSceneProject(CWorldEditor *editor, string file)
{
	world_editor = editor;
	InitEmptyScene();

	loaded = LoadFromFile(file);
}

CSceneProject::~CSceneProject(void)
{
	for(vector<CEditObject *>::iterator i=objects.begin(); i!=objects.end(); i++)
		delete *i;

	for(vector<CEditAsset *>::iterator i=assets.begin(); i!=assets.end(); i++)
		delete *i;
}

void CSceneProject::InitEmptyScene(void)
{
	filename = string();

	scene = new CEditScene(this);
}

void CSceneProject::AddObject(CEditObject *object)
{
	objects.push_back(object);
}

void CSceneProject::RemoveObject(CEditObject *object)
{
	for(vector<CEditObject *>::iterator i=objects.begin(); i!=objects.end(); i++)
	{
		if(*i==object)
		{
			objects.erase(i);
			return;
		}
	}
}

CEditObject *CSceneProject::FindObjectByName(string name, CEditObject *self)
{
	for(vector<CEditObject *>::iterator i=objects.begin(); i!=objects.end(); i++)
	{
		CEditObject *object = *i;
		if(object->GetName().compare(name) == 0 && *i != self)
			return object;
	}
	return 0;
}


void CSceneProject::AddAsset(CEditAsset *asset)
{
	assets.push_back(asset);
}

void CSceneProject::RemoveAsset(CEditAsset *asset)
{
	for(vector<CEditAsset *>::iterator i=assets.begin(); i!=assets.end(); i++)
	{
		if(*i == asset)
			assets.erase(i);
	}
}

CEditAsset *CSceneProject::FindAssetByName(string name, CEditAsset *self)
{
	if(name.size() == 0)
		return 0;

	for(vector<CEditAsset *>::iterator i=assets.begin(); i!=assets.end(); i++)
	{
		if((*i)->GetName().compare(name) == 0 && *i != self)
			return *i;
	}
	return 0;
}

string CSceneProject::GetUniqueObjectName(string name, CEditObject *self)
{
	string unique = name;
	int n = 0;
	char n_str[5];

	while(FindObjectByName(unique, self))
	{
		n++;
		snprintf(n_str, 5, "%04d", n);
		unique = name + string(".") + string("") + string(n_str);
	}

	return unique;
}

string CSceneProject::GetUniqueAssetName(string name, CEditAsset *self)
{
	string unique = name;
	int n = 0;
	char n_str[5];

	while(FindAssetByName(unique, self))
	{
		n++;
		snprintf(n_str, 5, "%04d", n);
		unique = name + string(".") + string("") + string(n_str);
	}

	return unique;
}





void CSceneProject::AssetContentChanged(CEditAsset *asset)
{
	switch(asset->GetType())
	{
		case EDIT_ASSET_TYPE_MESH:
			MeshAssetContentChanged((CMeshEditAsset *)asset);
			break;
		case EDIT_ASSET_TYPE_CUBE_MAP:
			CubeMapAssetContentChanged((CCubeMapEditAsset *)asset);
			break;
	}
}

void CSceneProject::MeshAssetContentChanged(CMeshEditAsset *asset)
{
	union
	{
			CEditObject *object;
			CMeshEditObject *mesh_object;
	};

	for(vector<CEditObject *>::iterator i=objects.begin(); i!=objects.end(); i++)
	{
		object = *i;
		switch(object->GetType())
		{
			case EDIT_OBJECT_TYPE_MESH:
				if(mesh_object->GetMeshAsset() == asset)
					mesh_object->MeshAssetContentChanged();
				break;
		}
	}
}

void CSceneProject::CubeMapAssetContentChanged(CCubeMapEditAsset *asset)
{
}


void CSceneProject::ObjectNameChanged(CEditObject *object)
{
	world_editor->ObjectNameChanged(object);
}

void CSceneProject::ObjectTransformChanged(CEditObject *object)
{
	world_editor->ObjectTransformChanged(object);
}

void CSceneProject::AssetNameChanged(CEditAsset *asset)
{
	world_editor->AssetNameChanged(asset);
}



CEditObject *CSceneProject::FindObjectByPointer(void *p)
{
	CEditObject *object;

	for(vector<CEditObject *>::iterator i=objects.begin(); i!=objects.end(); i++)
	{
		object = *i;
		if(object == p)
			return object;
	}

	return 0;
}




