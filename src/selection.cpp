#include "selection.h"

CSelection::CSelection(void)
{
	Clear();
}

void CSelection::Clear(void)
{
	locked = false;
	type = SELECTION_TYPE_EMPTY;
}

void CSelection::Set(CEditObject *single_object)
{
	locked = false;
	this->type = SELECTION_TYPE_SINGLE_OBJECT;
	this->single_object = single_object;
}

void CSelection::Set(CEditAsset *single_asset)
{
	locked = false;
	this->type = SELECTION_TYPE_SINGLE_ASSET;
	this->single_asset = single_asset;
}

void CSelection::SetType(int type)
{
	this->type = type;
}
