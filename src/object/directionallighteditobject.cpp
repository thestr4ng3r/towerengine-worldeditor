#include "allincludes.h"

CDirectionalLightEditObject::CDirectionalLightEditObject(CSceneProject *world, tVector dir, tVector color) : CEditObject(world)
{
	this->direction = dir;
	this->color = color;

	space_connection = 0;

	AddProperty(new CDirectionalLightEditObjectDirectionProperty(this));
	AddProperty(new CDirectionalLightEditObjectColorProperty(this));
}

CDirectionalLightEditObject::~CDirectionalLightEditObject(void)
{
}

CDirectionalLightEditObjectSpaceConnection *CDirectionalLightEditObject::CreateSpaceConnection(CEditSpace *space)
{
	space_connection = new CDirectionalLightEditObjectSpaceConnection(this, space);
	space_connection->Update();
	return space_connection;
}

// direction property

CDirectionalLightEditObjectDirectionProperty::CDirectionalLightEditObjectDirectionProperty(CDirectionalLightEditObject *object)
{
	this->object = object;
}

QWidget *CDirectionalLightEditObjectDirectionProperty::CreateEditWidget(CMainWindow *main_win)
{
	CVectorEditWidget *w = new CVectorEditWidget(QString("Direction"), object->GetDirection(), main_win);
	w->SetCallback(this);
	return w;
}

xmlNodePtr CDirectionalLightEditObjectDirectionProperty::CreateSceneProjectXMLNode(void)
{
	return CreateVectorXMLNode(tcxc("direction"), object->GetDirection());
}

xmlNodePtr CDirectionalLightEditObjectDirectionProperty::CreateSceneExportXMLNode(void)
{
	return CreateVectorXMLNode(tcxc("direction"), object->GetDirection());
}

void CDirectionalLightEditObjectDirectionProperty::ValueChanged(tVector v)
{
	object->SetDirection(v);
}

// color property

CDirectionalLightEditObjectColorProperty::CDirectionalLightEditObjectColorProperty(CDirectionalLightEditObject *object)
{
	this->object = object;
}

QWidget *CDirectionalLightEditObjectColorProperty::CreateEditWidget(CMainWindow *main_win)
{
	CColorEditWidget *w = new CColorEditWidget(QString("Color"), object->GetColor(), main_win);
	w->SetCallback(this);
	return w;
}

xmlNodePtr CDirectionalLightEditObjectColorProperty::CreateSceneProjectXMLNode(void)
{
	return CreateVectorXMLNode(tcxc("color"), object->GetColor(), tcxc("r"), tcxc("g"), tcxc("b"));
}

xmlNodePtr CDirectionalLightEditObjectColorProperty::CreateSceneExportXMLNode(void)
{
	return CreateVectorXMLNode(tcxc("color"), object->GetColor(), tcxc("r"), tcxc("g"), tcxc("b"));
}

void CDirectionalLightEditObjectColorProperty::ValueChanged(tVector v)
{
	object->SetColor(v);
}


// space connection

CDirectionalLightEditObjectSpaceConnection::CDirectionalLightEditObjectSpaceConnection(CDirectionalLightEditObject *object, CEditSpace *space) : CEditObjectSpaceConnection(object, space)
{
	this->object = object;

	light = new tDirectionalLight(Vec(0.0, 0.0, 0.0), Vec(0.0, 0.0, 0.0));
	GetEditSpace()->GetWorld()->AddDirectionalLight(light);
}

CDirectionalLightEditObjectSpaceConnection::~CDirectionalLightEditObjectSpaceConnection(void)
{
	GetEditSpace()->GetWorld()->RemoveDirectionalLight(light);

	delete light;
}

void CDirectionalLightEditObjectSpaceConnection::Update(void)
{
	light->SetDirection(object->GetDirection());
	light->SetColor(object->GetColor());
}









