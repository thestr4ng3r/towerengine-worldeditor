#include "allincludes.h"


CMeshEditObject::CMeshEditObject(CSceneProject *world, CMeshEditAsset *mesh) : CEditObject(world)
{
	space_connection = 0;

	SetMeshAsset(mesh);
	SetTransform(CEditTransform());

	AddProperty(new CMeshEditObjectTransformProperty(this));
	AddProperty(new CMeshEditObjectMeshAssetProperty(this));

}

CMeshEditObject::~CMeshEditObject(void)
{
}


void CMeshEditObject::SetMeshAsset(CMeshEditAsset *mesh)
{
	if(mesh == this->mesh_asset)
		return;

	this->mesh_asset = mesh;

	if(space_connection)
		space_connection->UpdateMeshObject();
}

void CMeshEditObject::SetTransform(CEditTransform t)
{
	transform = t;
	if(space_connection)
		space_connection->UpdateMeshObjectProps();
	project->ObjectTransformChanged(this);
}

void CMeshEditObject::Move(tVector v)
{
	CEditTransform trans = transform;
	trans.SetPosition(trans.GetPosition() + v);
	SetTransform(trans);
}

void CMeshEditObject::MeshAssetContentChanged(void)
{
	if(space_connection)
		space_connection->UpdateMeshObject();
}

CMeshEditObjectSpaceConnection *CMeshEditObject::CreateSpaceConnection(CEditSpace *space)
{
	space_connection = new CMeshEditObjectSpaceConnection(this, space);
	space_connection->UpdateMeshObject();
	return space_connection;
}


// transform property

CMeshEditObjectTransformProperty::CMeshEditObjectTransformProperty(CMeshEditObject *object)
{
	this->object = object;
}

QWidget *CMeshEditObjectTransformProperty::CreateEditWidget(CMainWindow *main_win)
{
	CTransformEditWidget *w = new CTransformEditWidget(object->GetTransform(), main_win);
	w->SetCallback(this);
	return w;
}

void CMeshEditObjectTransformProperty::ValueChanged(CEditTransform t)
{
	object->SetTransform(t);
}

xmlNodePtr CMeshEditObjectTransformProperty::CreateSceneProjectXMLNode(void)
{
	xmlNodePtr n;

	n = CreateEditTransformXMLNode(tcxc("transform"), object->GetTransform());

	return n;
}

xmlNodePtr CMeshEditObjectTransformProperty::CreateSceneExportXMLNode(void)
{
	xmlNodePtr n;

	n = CreateTransformXMLNode(tcxc("transform"), object->GetTransform().GetTransform());

	return n;
}


CMeshEditObjectMeshAssetProperty::CMeshEditObjectMeshAssetProperty(CMeshEditObject *object)
{
	this->object = object;
}

QWidget *CMeshEditObjectMeshAssetProperty::CreateEditWidget(CMainWindow *main_win)
{
	CAssetEditWidget *w = new CAssetEditWidget(QString("Mesh"), object->GetSceneProject(), EDIT_ASSET_TYPE_MESH, main_win, object->GetMeshAsset());
	w->SetCallback(this);
	return w;
}

xmlNodePtr CMeshEditObjectMeshAssetProperty::CreateSceneProjectXMLNode(void)
{
	if(!object->GetMeshAsset())
		return 0;

	xmlNodePtr n;

	n = xmlNewNode(0, (const xmlChar *)"mesh_asset");
	xmlSetProp(n, (const xmlChar *)"asset", (const xmlChar *)object->GetMeshAsset()->GetName().c_str());

	return n;
}

xmlNodePtr CMeshEditObjectMeshAssetProperty::CreateSceneExportXMLNode(void)
{
	if(!object->GetMeshAsset())
		return 0;

	xmlNodePtr n;

	n = xmlNewNode(0, (const xmlChar *)"mesh_asset");
	xmlSetProp(n, (const xmlChar *)"asset", (const xmlChar *)object->GetMeshAsset()->GetName().c_str());

	return n;
}

void CMeshEditObjectMeshAssetProperty::ValueChanged(CEditAsset *v)
{
	if(v)
		if(v->GetType() != EDIT_ASSET_TYPE_MESH)
			return;

	object->SetMeshAsset((CMeshEditAsset *)v);
}


// space connection


CMeshEditObjectSpaceConnection::CMeshEditObjectSpaceConnection(CMeshEditObject *object, CEditSpace *space) : CEditObjectSpaceConnection(object, space)
{
	this->object = object;

	mesh_object = 0;

	collision_shape = new btEmptyShape();
	motion_state = new btDefaultMotionState();
	rigid_body = new btRigidBody(0.0, motion_state, collision_shape);
	rigid_body->setUserPointer(object);
	space->GetPhysicsWorld()->addRigidBody(rigid_body);
}

CMeshEditObjectSpaceConnection::~CMeshEditObjectSpaceConnection(void)
{
	if(mesh_object)
	{
		GetEditSpace()->GetWorld()->RemoveObject(mesh_object);
		delete mesh_object;
	}

	GetEditSpace()->GetPhysicsWorld()->removeRigidBody(rigid_body);

	delete rigid_body;
	delete motion_state;
	delete collision_shape;
}

void CMeshEditObjectSpaceConnection::UpdateMeshObject(void)
{
	tWorld *world = GetEditSpace()->GetWorld();

	if(mesh_object)
	{
		world->RemoveObject(mesh_object);
		delete mesh_object;
		mesh_object = 0;
	}

	CMeshEditAsset *mesh_asset = object->GetMeshAsset();

	if(mesh_asset)
	{
		if(mesh_asset->GetMesh())
			mesh_object = new tMeshObject(mesh_asset->GetMesh());
	}

	btCollisionShape *old_shape = collision_shape;

	if(mesh_object)
	{
		world->AddObject(mesh_object);
		collision_shape = new btBvhTriangleMeshShape(mesh_asset->GetMesh()->GetPhysicsMesh(), true);
		rigid_body->setCollisionShape(collision_shape);
		UpdateMeshObjectProps();
	}
	else
	{
		collision_shape = new btEmptyShape();
		rigid_body->setCollisionShape(collision_shape);
	}

	delete old_shape;
}

void CMeshEditObjectSpaceConnection::UpdateMeshObjectProps(void)
{
	if(!mesh_object)
		return;

	mesh_object->SetTransform(object->GetTransform().GetTransform());
	mesh_object->UpdateRigidBodyTransformation();

	rigid_body->setWorldTransform(object->GetTransform().GetTransform().ToBtTransform());
}









