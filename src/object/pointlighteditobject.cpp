#include "allincludes.h"


CPointLightEditObject::CPointLightEditObject(CSceneProject *world, tVector pos, tVector color, float dist) : CEditObject(world)
{
	this->position = pos;
	this->color = color;
	this->distance = dist;

	space_connection = 0;

	AddProperty(new CPointLightEditObjectPositionProperty(this));
	AddProperty(new CPointLightEditObjectDistanceProperty(this));
	AddProperty(new CPointLightEditObjectColorProperty(this));

}

CPointLightEditObject::~CPointLightEditObject(void)
{
}

CPointLightEditObjectSpaceConnection *CPointLightEditObject::CreateSpaceConnection(CEditSpace *space)
{
	space_connection = new CPointLightEditObjectSpaceConnection(this, space);
	space_connection->Update();
	return space_connection;
}

// ------------------------------------------

CPointLightEditObjectPositionProperty::CPointLightEditObjectPositionProperty(CPointLightEditObject *object)
{
	this->object = object;
}

QWidget *CPointLightEditObjectPositionProperty::CreateEditWidget(CMainWindow *main_win)
{
	CVectorEditWidget *w = new CVectorEditWidget(QString("Position"), object->GetPosition(), main_win);
	w->SetCallback(this);
	return w;
}

xmlNodePtr CPointLightEditObjectPositionProperty::CreateSceneProjectXMLNode(void)
{
	return CreateVectorXMLNode((const xmlChar *)"position", object->GetPosition());
}

xmlNodePtr CPointLightEditObjectPositionProperty::CreateSceneExportXMLNode(void)
{
	return CreateVectorXMLNode((const xmlChar *)"position", object->GetPosition());
}

void CPointLightEditObjectPositionProperty::ValueChanged(tVector v)
{
	object->SetPosition(v);
}

// ------------------------------------------

CPointLightEditObjectColorProperty::CPointLightEditObjectColorProperty(CPointLightEditObject *object)
{
	this->object = object;
}

QWidget *CPointLightEditObjectColorProperty::CreateEditWidget(CMainWindow *main_win)
{
	CColorEditWidget *w = new CColorEditWidget(QString("Color"), object->GetColor(), main_win);
	w->SetCallback(this);
	return w;
}

xmlNodePtr CPointLightEditObjectColorProperty::CreateSceneProjectXMLNode(void)
{
	return CreateVectorXMLNode((const xmlChar *)"color", object->GetColor(), (const xmlChar *)"r",  (const xmlChar *)"g",  (const xmlChar *)"b");
}

xmlNodePtr CPointLightEditObjectColorProperty::CreateSceneExportXMLNode(void)
{
	return CreateVectorXMLNode((const xmlChar *)"color", object->GetColor(), (const xmlChar *)"r",  (const xmlChar *)"g",  (const xmlChar *)"b");
}

void CPointLightEditObjectColorProperty::ValueChanged(tVector v)
{
	object->SetColor(v);
}

// ------------------------------------------

CPointLightEditObjectDistanceProperty::CPointLightEditObjectDistanceProperty(CPointLightEditObject *object)
{
	this->object = object;
}

QWidget *CPointLightEditObjectDistanceProperty::CreateEditWidget(CMainWindow *main_win)
{	
	CFloatEditWidget *w = new CFloatEditWidget(QString("Distance"), object->GetDistance(), 0.0, INFINITY, main_win);
	w->SetCallback(this);
	return w;
}

xmlNodePtr CPointLightEditObjectDistanceProperty::CreateSceneProjectXMLNode(void)
{
	return CreateFloatXMLNode((const xmlChar *)"distance", object->GetDistance());
}

xmlNodePtr CPointLightEditObjectDistanceProperty::CreateSceneExportXMLNode(void)
{
	return CreateFloatXMLNode((const xmlChar *)"distance", object->GetDistance());
}

void CPointLightEditObjectDistanceProperty::ValueChanged(float v)
{
	object->SetDistance(v);
}


// ------------------------------------------
// space connection

CPointLightEditObjectSpaceConnection::CPointLightEditObjectSpaceConnection(CPointLightEditObject *object, CEditSpace *space) : CEditObjectSpaceConnection(object, space)
{
	this->object = object;

	light = new tPointLight(Vec(0.0, 0.0, 0.0), Vec(0.0, 0.0, 0.0), 0.0);
	space->GetWorld()->AddPointLight(light);

	collision_shape = new btSphereShape(0.3);
	motion_state = new btDefaultMotionState();
	rigid_body = new btRigidBody(0.0, motion_state, collision_shape);
	rigid_body->setUserPointer(object);
	space->GetPhysicsWorld()->addRigidBody(rigid_body);
}

CPointLightEditObjectSpaceConnection::~CPointLightEditObjectSpaceConnection(void)
{
	GetEditSpace()->GetWorld()->RemovePointLight(light);
	delete light;

	GetEditSpace()->GetPhysicsWorld()->removeRigidBody(rigid_body);

	delete rigid_body;
	delete motion_state;
	delete collision_shape;
}

void CPointLightEditObjectSpaceConnection::Update(void)
{
	light->SetPosition(object->GetPosition());
	light->SetColor(object->GetColor());
	light->SetDistance(object->GetDistance());

	btTransform trans = btTransform::getIdentity();
	trans.setOrigin(BtVec(object->GetPosition()));
	rigid_body->setWorldTransform(trans);
}
