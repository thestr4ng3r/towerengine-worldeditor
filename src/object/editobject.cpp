
#include "allincludes.h"

CEditObject::CEditObject(CSceneProject *project)
{
	name = string();
	this->project = project;

	AddProperty(new CEditObjectNameProperty(this));
}

void CEditObject::SetName(string name)
{
	this->name = project->GetUniqueObjectName(name, this);
	project->ObjectNameChanged(this);
}

xmlNodePtr CEditObject::CreateSceneProjectXMLNode(void)
{
	xmlNodePtr n = xmlNewNode(0, GetSceneProjectXMLNodeName());
	xmlNodePtr child;

	xmlSetProp(n, tcxc("name"), tcxc(GetName().c_str()));

	for(int i=0; i<GetPropertiesCount(); i++)
	{
		child = GetProperty(i)->CreateSceneProjectXMLNode();
		if(!child)
			continue;
		xmlAddChild(n, child);
	}

	return n;
}

xmlNodePtr CEditObject::CreateSceneExportXMLNode(void)
{
	xmlNodePtr n = xmlNewNode(0, GetSceneExportXMLNodeName());
	xmlNodePtr child;

	xmlSetProp(n, tcxc("name"), tcxc(GetName().c_str()));

	for(int i=0; i<GetPropertiesCount(); i++)
	{
		child = GetProperty(i)->CreateSceneExportXMLNode();
		if(!child)
			continue;
		xmlAddChild(n, child);
	}

	return n;
}



CEditObjectNameProperty::CEditObjectNameProperty(CEditObject *object)
{
	this->object = object;
}

QWidget *CEditObjectNameProperty::CreateEditWidget(CMainWindow *main_win)
{
	CStringEditWidget *w = new CStringEditWidget(QString("Name"), object->GetName(), main_win);
	w->SetCallback(this);
	return w;
}

void CEditObjectNameProperty::ValueChanged(string s, string *c)
{
	if(object->GetName() == s)
		return;

	object->SetName(s);
	*c = object->GetName();
}

