#include "allincludes.h"

#include <QMessageBox>

CWorldEditor::CWorldEditor()
{
	selection = new CSelection();

	edit_space = new CEditSpace(this);
	scene_project = new CSceneProject(this);

	main_window = new CMainWindow(this);
	main_window->InitialUpdate();
	main_window->show();
}


void CWorldEditor::SelectObject(CEditObject *object)
{
	selection->Set(object);
	main_window->SelectionChanged();
}

void CWorldEditor::SelectAsset(CEditAsset *asset)
{
	selection->Set(asset);
	main_window->SelectionChanged();
}

void CWorldEditor::SelectScene(void)
{
	selection->SetType(SELECTION_TYPE_SCENE);
	main_window->SelectionChanged();
}

void CWorldEditor::ClearSelection(void)
{
	selection->Clear();
	main_window->SelectionChanged();
}


void CWorldEditor::ExportTowerEngineScene(string file)
{
	scene_project->ExportTowerEngineScene(file);
}

bool CWorldEditor::SaveSceneProject(string file)
{
	return scene_project->SaveSceneProject(file);
}

void CWorldEditor::LoadSceneProject(string file)
{
	main_window->InitSceneProjectChange();
	main_window->StartLoadingProject();

	CSceneProject *loaded_project = new CSceneProject(this, file);
	if(!loaded_project->GetLoaded())
	{
		string msg = string("Failed to load file \"") + file + string("\"");
		QMessageBox::critical(main_window, QString("Load Project"), QString::fromStdString(msg));
		delete loaded_project;
	}
	else
	{
		edit_space->Clear();
		delete scene_project;
		scene_project = loaded_project;
		edit_space->SetFromProject(scene_project);
		main_window->UpdateSceneInterface();
	}

	main_window->FinishLoadingProject();
}

void CWorldEditor::ObjectNameChanged(CEditObject *object)
{
	main_window->ObjectNameChanged(object);
}

void CWorldEditor::ObjectTransformChanged(CEditObject *object)
{
	main_window->ObjectTransformChanged(object);
}

void CWorldEditor::AssetNameChanged(CEditAsset *asset)
{
	main_window->AssetNameChanged(asset);
}
