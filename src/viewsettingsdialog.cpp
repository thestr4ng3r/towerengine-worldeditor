#include "allincludes.h"

CViewSettingsDialog::CViewSettingsDialog(CMainWindow *parent, CEditSpace *edit_space) : QDialog(parent), Ui::ViewSettingsDialog()
{
	setupUi(this);

	setAttribute(Qt::WA_DeleteOnClose);

	main_win = parent;
	this->edit_space = edit_space;

	connect(CameraAngleSlider, SIGNAL(valueChanged(int)), this, SLOT(UpdateCameraAngleLabel()));

	CameraMoveSpeedNormalSpinBox->setValue(edit_space->GetCamSpeedNormal());
	CameraMoveSpeedFastSpinBox->setValue(edit_space->GetCamSpeedFast());
	CameraAngleSlider->setValue(edit_space->GetCamAngle());
}

CViewSettingsDialog::~CViewSettingsDialog(void)
{
	main_win->setEnabled(true);
}

void CViewSettingsDialog::accept(void)
{
	edit_space->SetViewSettings(CameraMoveSpeedNormalSpinBox->value(),
								CameraMoveSpeedFastSpinBox->value(),
								CameraAngleSlider->value());
	QDialog::accept();
}

void CViewSettingsDialog::UpdateCameraAngleLabel(void)
{
	CameraAngleValueLabel->setText(QString::number(CameraAngleSlider->value()));
}
