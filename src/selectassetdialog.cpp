#include "allincludes.h"

CSelectAssetDialog::CSelectAssetDialog(CAssetEditWidget *edit_widget, CMainWindow *main_win, QWidget *parent) : QDialog(parent), Ui::SelectAssetDialog()
{
	setupUi(this);

	setAttribute(Qt::WA_DeleteOnClose);

	this->edit_widget = edit_widget;
	this->main_win = main_win;

	UpdateAssetsList();

	connect(AssetsListWidget, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(accept()));
}

CSelectAssetDialog::~CSelectAssetDialog()
{
	main_win->setEnabled(true);
}

void CSelectAssetDialog::UpdateAssetsList(void)
{
	AssetsListWidget->clear();

	int i;
	CMeshEditAsset *asset;
	QListWidgetItem *item;
	CSceneProject *project = edit_widget->GetProject();

	item = new QListWidgetItem(AssetsListWidget);
	item->setText(tr("<none>"));
	item->setData(Qt::UserRole, QVariant(-1));

	for(i=0; i<project->GetAssetsCount(); i++)
	{
		asset = (CMeshEditAsset *)project->GetAsset(i);
		if(asset->GetType() != edit_widget->GetType())
			continue;
		item = new QListWidgetItem(AssetsListWidget);
		item->setText(QString(asset->GetName().data()));
		item->setIcon(*CMainWindow::GetAssetTypeIcon(asset->GetType()));
		item->setData(Qt::UserRole, QVariant(i)); // TODO: save pointer instead
		AssetsListWidget->addItem(item);
	}
}

void CSelectAssetDialog::accept(void)
{
	CEditAsset *asset = 0;

	if(AssetsListWidget->selectedItems().count() == 1)
	{
		int d = AssetsListWidget->selectedItems().at(0)->data(Qt::UserRole).toInt();
		if(d >= 0)
			asset = edit_widget->GetProject()->GetAsset(d);
	}

	edit_widget->ChangeValue(asset);

	QDialog::accept();
}
