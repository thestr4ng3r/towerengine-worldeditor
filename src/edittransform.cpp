#include "allincludes.h"

CEditTransform::CEditTransform(tVector position, tVector rotation, tVector scale)
{
	this->position = position;
	this->rotation = rotation;
	this->scale = scale;
}

CEditTransform::CEditTransform(void)
{
	position = Vec(0.0, 0.0, 0.0);
	rotation = Vec(0.0, 0.0, 0.0);
	scale = Vec(1.0, 1.0, 1.0);
}

tTransform CEditTransform::GetTransform(void)
{
	tMatrix3 m;
	m.SetEuler((rotation * (1.0 / 180.0)) * M_PI);
	m.Scale(scale);
	return tTransform(m, position);
}
