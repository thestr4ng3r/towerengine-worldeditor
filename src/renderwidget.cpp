#include "allincludes.h"

#include "renderwidget.h"

#include <QMouseEvent>

/*class CVectorTestObject : public tObject
{
	private:
		tVector a, b;

	public:
		CVectorTestObject(tVector a, tVector b);

		void ForwardPass(void);
		tBoundingBox GetBoundingBox(void);
};

CVectorTestObject::CVectorTestObject(tVector a, tVector b)
{
	this->a = a;
	this->b = b;
}

void CVectorTestObject::ForwardPass(void)
{
	tShader::Unbind();

	glLineWidth(3.0);
	glColor4f(0.0, 1.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);

	glBegin(GL_LINES);
	a.PutToGL();
	b.PutToGL();
	glEnd();
}

tBoundingBox CVectorTestObject::GetBoundingBox(void)
{
	tBoundingBox box;
	box.AddPoint(a);
	box.AddPoint(b);
	return box;
}*/

CRenderWidget::CRenderWidget(CWorldEditor *world_editor, QWidget *parent) : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
	this->world_editor = world_editor;
	up_pressed = down_pressed = left_pressed = right_pressed = fast_pressed = false;

	camera_rotate_mode = false;
}

void CRenderWidget::initializeGL(void)
{
	world_editor->GetEditSpace()->Init(this->width(), this->height());

	startTimer(16);
}

void CRenderWidget::resizeGL(int width, int height)
{
	world_editor->GetEditSpace()->SetScreenSize(width, height);
}

/*void CRenderWidget::paintGL()
{
}*/

void CRenderWidget::paintEvent(QPaintEvent *event)
{
	makeCurrent();
	world_editor->GetEditSpace()->Render();
	swapBuffers();
}

void CRenderWidget::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::RightButton)
	{
		setCursor(Qt::BlankCursor);
		last_mouse_x = width() / 2;
		last_mouse_y = height() / 2;
		camera_rotate_mode = true;
		camera_rotate_delay = 4;
		initial_mouse_pos = event->globalPos();
		QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
	}
	else
	{
		if(event->button() == Qt::LeftButton)
		{
			float x = ((float)event->x() / (float)width()) * 2.0 - 1.0;
			float y = ((float)event->y() / (float)height()) * -2.0 + 1.0;

			world_editor->GetEditSpace()->MouseDown(x, y);
		}

		last_mouse_x = event->x();
		last_mouse_y = event->y();
	}

	QGLWidget::mousePressEvent(event);
}

void CRenderWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::RightButton && camera_rotate_mode)
	{
		setCursor(Qt::ArrowCursor);
		camera_rotate_mode = false;
		QCursor::setPos(initial_mouse_pos);
	}

	if(event->button() == Qt::LeftButton)
	{
		world_editor->GetEditSpace()->MouseUp();
	}
}

void CRenderWidget::mouseMoveEvent(QMouseEvent *event)
{
	static const float cam_rot_speed = 0.0015;

	float delta_x = (float)(event->x() - last_mouse_x);
	float delta_y = (float)(event->y() - last_mouse_y);

	if(camera_rotate_mode && event->buttons() & Qt::RightButton)
	{
		if(camera_rotate_delay > 0)
			camera_rotate_delay--;
		else
		{
			world_editor->GetEditSpace()->RotateCamera(delta_x * cam_rot_speed, -delta_y * cam_rot_speed);
		}

		last_mouse_x = width() / 2;
		last_mouse_y = height() / 2;

		QCursor::setPos(mapToGlobal(QPoint(width() / 2, height() / 2)));
	}
	else
	{
		if(event->buttons() & Qt::LeftButton)
		{
			float dx = (delta_x / (float)width()) * 2.0;
			float dy = (delta_y / (float)height()) * -2.0;

			world_editor->GetEditSpace()->MouseDrag(dx, dy);
		}

		last_mouse_x = event->x();
		last_mouse_y = event->y();
	}

	QGLWidget::mouseMoveEvent(event);
}

void CRenderWidget::keyPressEvent(QKeyEvent *event)
{
	switch(event->key())
	{
		case Qt::Key_W:
			up_pressed = true;
			break;
		case Qt::Key_S:
			down_pressed = true;
			break;
		case Qt::Key_A:
			left_pressed = true;
			break;
		case Qt::Key_D:
			right_pressed = true;
			break;
		case Qt::Key_Shift:
			fast_pressed = true;
			break;
	}
}

void CRenderWidget::keyReleaseEvent(QKeyEvent *event)
{
	switch(event->key())
	{
		case Qt::Key_W:
			up_pressed = false;
			break;
		case Qt::Key_S:
			down_pressed = false;
			break;
		case Qt::Key_A:
			left_pressed = false;
			break;
		case Qt::Key_D:
			right_pressed = false;
			break;
		case Qt::Key_Shift:
			fast_pressed = false;
			break;
	}
}

void CRenderWidget::timerEvent(QTimerEvent *event)
{
	float move_x, move_z;

	move_x = (left_pressed ? 1.0 : 0.0) + (right_pressed ? -1.0 : 0.0);
	move_z = (up_pressed ? 1.0 : 0.0) + (down_pressed ? -1.0 : 0.0);

	world_editor->GetEditSpace()->MoveCamera(move_x, move_z, fast_pressed);

	/*if(up_pressed)
		world_editor->MoveCamera(0.0, move_speed);
	if(down_pressed)
		world_editor->MoveCamera(0.0, -move_speed);
	if(left_pressed)
		world_editor->MoveCamera(move_speed, 0.0);
	if(right_pressed)
		world_editor->MoveCamera(-move_speed, 0.0);*/

	repaint();
}
