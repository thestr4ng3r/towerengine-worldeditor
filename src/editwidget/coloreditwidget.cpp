#include "allincludes.h"

CColorEditWidget::CColorEditWidget(QString name, tVector v, QWidget *parent) : QWidget(parent), Ui::ColorEditWidget()
{
	setupUi(this);

	callback = 0;

	QValidator *val = new QDoubleValidator(0.0, INFINITY, 10, this);
	REdit->setValidator(val);
	GEdit->setValidator(val);
	BEdit->setValidator(val);

	NameLabel->setText(name + tr(":"));

	SetValue(v);

	connect(REdit, SIGNAL(editingFinished()), this, SLOT(REditFinished()));
	connect(GEdit, SIGNAL(editingFinished()), this, SLOT(GEditFinished()));
	connect(BEdit, SIGNAL(editingFinished()), this, SLOT(BEditFinished()));
}

CColorEditWidget::~CColorEditWidget()
{
}

void CColorEditWidget::SetValue(tVector v)
{
	REdit->setText(QString::number((double)v.x));
	GEdit->setText(QString::number((double)v.y));
	BEdit->setText(QString::number((double)v.z));
}

tVector CColorEditWidget::GetValue(void)
{
	return Vec(REdit->text().replace(',', '.').toFloat(),
			   GEdit->text().replace(',', '.').toFloat(),
			   BEdit->text().replace(',', '.').toFloat());
}

void CColorEditWidget::TriggerValueChanged(tVector v)
{
	emit ValueChanged(v);

	if(callback)
		callback->ValueChanged(v);
}

void CColorEditWidget::REditFinished(void)
{
	tVector v = GetValue();
	REdit->setText(QString::number((double)v.x));
	TriggerValueChanged(v);
}

void CColorEditWidget::GEditFinished(void)
{
	tVector v = GetValue();
	GEdit->setText(QString::number((double)v.y));
	TriggerValueChanged(v);
}

void CColorEditWidget::BEditFinished(void)
{
	tVector v = GetValue();
	BEdit->setText(QString::number((double)v.z));
	TriggerValueChanged(v);
}
