#include "allincludes.h"

CStringEditWidget::CStringEditWidget(QString name, string v, QWidget *parent) : QWidget(parent), Ui::StringEditWidget()
{
	setupUi(this);

	callback = 0;

	NameLabel->setText(name + tr(":"));
	SetValue(v);

	connect(ValueEdit, SIGNAL(editingFinished()), this, SLOT(EditFinished()));
}

CStringEditWidget::~CStringEditWidget()
{
}

void CStringEditWidget::SetValue(string v)
{
	ValueEdit->setText(QString::fromStdString(v));
}

string CStringEditWidget::GetValue(void)
{
	return ValueEdit->text().toStdString();
}

void CStringEditWidget::EditFinished(void)
{
	string c = GetValue();

	emit ValueChanged(GetValue(), &c);
	if(callback)
		callback->ValueChanged(GetValue(), &c);

	SetValue(c);
}
