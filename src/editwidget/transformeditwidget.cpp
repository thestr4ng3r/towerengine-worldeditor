#include "allincludes.h"

CTransformEditWidget::CTransformEditWidget(CEditTransform t, QWidget *parent) : QWidget(parent), Ui::TransformEditWidget()
{
	setupUi(this);

	callback = 0;

	QValidator *val = new QDoubleValidator(-INFINITY, INFINITY, 10, this);
	PositionXEdit->setValidator(val);
	PositionYEdit->setValidator(val);
	PositionZEdit->setValidator(val);

	RotationXEdit->setValidator(val);
	RotationYEdit->setValidator(val);
	RotationZEdit->setValidator(val);

	ScaleXEdit->setValidator(val);
	ScaleYEdit->setValidator(val);
	ScaleZEdit->setValidator(val);

	connect(PositionXEdit, SIGNAL(editingFinished()), this, SLOT(PositionXEditFinished()));
	connect(PositionYEdit, SIGNAL(editingFinished()), this, SLOT(PositionYEditFinished()));
	connect(PositionZEdit, SIGNAL(editingFinished()), this, SLOT(PositionZEditFinished()));

	connect(RotationXEdit, SIGNAL(editingFinished()), this, SLOT(RotationXEditFinished()));
	connect(RotationYEdit, SIGNAL(editingFinished()), this, SLOT(RotationYEditFinished()));
	connect(RotationZEdit, SIGNAL(editingFinished()), this, SLOT(RotationZEditFinished()));

	connect(ScaleXEdit, SIGNAL(editingFinished()), this, SLOT(ScaleXEditFinished()));
	connect(ScaleYEdit, SIGNAL(editingFinished()), this, SLOT(ScaleYEditFinished()));
	connect(ScaleZEdit, SIGNAL(editingFinished()), this, SLOT(ScaleZEditFinished()));

	SetValue(t);
}

CTransformEditWidget::~CTransformEditWidget()
{
}

void CTransformEditWidget::SetValue(CEditTransform t)
{
	PositionXEdit->setText(QString::number((double)t.GetPosition().x));
	PositionYEdit->setText(QString::number((double)t.GetPosition().y));
	PositionZEdit->setText(QString::number((double)t.GetPosition().z));

	RotationXEdit->setText(QString::number((double)t.GetRotation().x));
	RotationYEdit->setText(QString::number((double)t.GetRotation().y));
	RotationZEdit->setText(QString::number((double)t.GetRotation().z));

	ScaleXEdit->setText(QString::number((double)t.GetScale().x));
	ScaleYEdit->setText(QString::number((double)t.GetScale().y));
	ScaleZEdit->setText(QString::number((double)t.GetScale().z));
}

CEditTransform CTransformEditWidget::GetValue(void)
{
	CEditTransform r;

	r.SetPosition(Vec(PositionXEdit->text().replace(',', '.').toFloat(),
					  PositionYEdit->text().replace(',', '.').toFloat(),
					  PositionZEdit->text().replace(',', '.').toFloat()));

	r.SetRotation(Vec(RotationXEdit->text().replace(',', '.').toFloat(),
					  RotationYEdit->text().replace(',', '.').toFloat(),
					  RotationZEdit->text().replace(',', '.').toFloat()));

	r.SetScale(Vec(	ScaleXEdit->text().replace(',', '.').toFloat(),
					ScaleYEdit->text().replace(',', '.').toFloat(),
					ScaleZEdit->text().replace(',', '.').toFloat()));

	return r;
}

void CTransformEditWidget::TriggerValueChanged(CEditTransform t)
{
	emit ValueChanged(t);
	if(callback)
		callback->ValueChanged(t);
}

void CTransformEditWidget::PositionXEditFinished(void)
{
	CEditTransform t = GetValue();
	PositionXEdit->setText(QString::number((double)t.GetPosition().x));
	TriggerValueChanged(t);
}

void CTransformEditWidget::PositionYEditFinished(void)
{
	CEditTransform t = GetValue();
	PositionYEdit->setText(QString::number((double)t.GetPosition().y));
	TriggerValueChanged(t);
}

void CTransformEditWidget::PositionZEditFinished(void)
{
	CEditTransform t = GetValue();
	PositionZEdit->setText(QString::number((double)t.GetPosition().z));
	TriggerValueChanged(t);
}

void CTransformEditWidget::RotationXEditFinished(void)
{
	CEditTransform t = GetValue();
	RotationXEdit->setText(QString::number((double)t.GetRotation().x));
	TriggerValueChanged(t);
}

void CTransformEditWidget::RotationYEditFinished(void)
{
	CEditTransform t = GetValue();
	RotationYEdit->setText(QString::number((double)t.GetRotation().y));
	TriggerValueChanged(t);
}

void CTransformEditWidget::RotationZEditFinished(void)
{
	CEditTransform t = GetValue();
	RotationZEdit->setText(QString::number((double)t.GetRotation().z));
	TriggerValueChanged(t);
}

void CTransformEditWidget::ScaleXEditFinished(void)
{
	CEditTransform t = GetValue();
	ScaleXEdit->setText(QString::number((double)t.GetScale().x));
	TriggerValueChanged(t);
}

void CTransformEditWidget::ScaleYEditFinished(void)
{
	CEditTransform t = GetValue();
	ScaleYEdit->setText(QString::number((double)t.GetScale().y));
	TriggerValueChanged(t);
}

void CTransformEditWidget::ScaleZEditFinished(void)
{
	CEditTransform t = GetValue();
	ScaleZEdit->setText(QString::number((double)t.GetScale().z));
	TriggerValueChanged(t);
}

