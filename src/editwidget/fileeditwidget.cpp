#include "allincludes.h"

#include <QFileDialog>

CFileEditWidget::CFileEditWidget(QString name, string filename, QString name_filter, QWidget *main_win, QWidget *parent) : QWidget(parent), Ui::FileEditWidget()
{
	setupUi(this);

	callback = 0;

	this->main_win = main_win;
	this->name_filter = name_filter;

	NameLabel->setText(name);
	FileEdit->setText(QString::fromStdString(filename));

	connect(OpenButton, SIGNAL(clicked()), this, SLOT(OpenFile()));
	connect(FileEdit, SIGNAL(editingFinished()), this, SLOT(EditFinished()));
}

CFileEditWidget::~CFileEditWidget()
{
}

void CFileEditWidget::OpenFile(void)
{
	QFileDialog dialog(main_win);

	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setNameFilter(name_filter);
	dialog.setViewMode(QFileDialog::List);
	dialog.selectFile(FileEdit->text());

	if(dialog.exec())
	{
		FileEdit->setText(dialog.selectedFiles().at(0));
		EditFinished();
	}
}

void CFileEditWidget::SetValue(string v)
{
	FileEdit->setText(QString::fromStdString(v));
}

string CFileEditWidget::GetValue(void)
{
	return FileEdit->text().toStdString();
}

void CFileEditWidget::EditFinished(void)
{
	string v = GetValue();

	emit ValueChanged(v);

	if(callback)
		callback->ValueChanged(v);
}
