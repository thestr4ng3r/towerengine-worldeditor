#include "floateditwidget.h"
#include "ui_floateditwidget.h"

CFloatEditWidget::CFloatEditWidget(QString name, float v, float min, float max, QWidget *parent) : QWidget(parent), Ui::FloatEditWidget()
{
	setupUi(this);

	callback = 0;

	QValidator *val = new QDoubleValidator(min, max, 10, this);
	ValueEdit->setValidator(val);

	NameLabel->setText(name + tr(":"));

	SetValue(v);

	connect(ValueEdit, SIGNAL(editingFinished()), this, SLOT(EditFinished()));

}

CFloatEditWidget::~CFloatEditWidget()
{
}

void CFloatEditWidget::SetValue(float v)
{
	ValueEdit->setText(QString::number((double)v));
}

float CFloatEditWidget::GetValue(void)
{
	return ValueEdit->text().replace(',', '.').toFloat();
}

void CFloatEditWidget::EditFinished(void)
{
	float v = GetValue();
	ValueEdit->setText(QString::number((double)v));

	emit ValueChanged(v);
	if(callback)
		callback->ValueChanged(v);
}
