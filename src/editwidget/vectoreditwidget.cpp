#include "allincludes.h"

CVectorEditWidget::CVectorEditWidget(QString name, tVector v, QWidget *parent) : QWidget(parent), Ui::VectorEditWidget()
{
	setupUi(this);

	callback = 0;

	QValidator *val = new QDoubleValidator(-INFINITY, INFINITY, 10, this);
	XEdit->setValidator(val);
	YEdit->setValidator(val);
	ZEdit->setValidator(val);

	NameLabel->setText(name + tr(":"));

	SetValue(v);

	connect(XEdit, SIGNAL(editingFinished()), this, SLOT(XEditFinished()));
	connect(YEdit, SIGNAL(editingFinished()), this, SLOT(YEditFinished()));
	connect(ZEdit, SIGNAL(editingFinished()), this, SLOT(ZEditFinished()));
}

CVectorEditWidget::~CVectorEditWidget()
{
}

void CVectorEditWidget::SetValue(tVector v)
{
	XEdit->setText(QString::number((double)v.x));
	YEdit->setText(QString::number((double)v.y));
	ZEdit->setText(QString::number((double)v.z));
}

tVector CVectorEditWidget::GetValue(void)
{
	return Vec(XEdit->text().replace(',', '.').toFloat(),
			   YEdit->text().replace(',', '.').toFloat(),
			   ZEdit->text().replace(',', '.').toFloat());
}

void CVectorEditWidget::TriggerValueChanged(tVector v)
{
	emit ValueChanged(v);
	if(callback)
		callback->ValueChanged(v);
}

void CVectorEditWidget::XEditFinished(void)
{
	tVector v = GetValue();
	XEdit->setText(QString::number((double)v.x));
	TriggerValueChanged(v);
}

void CVectorEditWidget::YEditFinished(void)
{
	tVector v = GetValue();
	YEdit->setText(QString::number((double)v.y));
	TriggerValueChanged(v);
}

void CVectorEditWidget::ZEditFinished(void)
{
	tVector v = GetValue();
	ZEdit->setText(QString::number((double)v.z));
	TriggerValueChanged(v);
}
