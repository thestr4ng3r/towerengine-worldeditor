#include "allincludes.h"

CAssetEditWidget::CAssetEditWidget(QString name, CSceneProject *project, int asset_type, CMainWindow *main_win, CEditAsset *asset, QWidget *parent) : QWidget(parent), Ui::AssetEditWidget()
{
	setupUi(this);

	callback = 0;

	this->project = project;
	this->type = asset_type;
	this->main_win = main_win;

	SetValue(asset);

	NameLabel->setText(name + tr(":"));

	connect(SelectAssetButton, SIGNAL(clicked()), this, SLOT(SelectAsset()));
}

CAssetEditWidget::~CAssetEditWidget()
{
}

void CAssetEditWidget::SetValue(CEditAsset *asset)
{
	if(!asset)
		this->asset = 0;
	else
	{
		if(asset->GetType() == type)
			this->asset = asset;
		else
			this->asset = 0;
	}

	if(this->asset)
		AssetNameEdit->setText(QString::fromStdString(this->asset->GetName()));
	else
		AssetNameEdit->setText(tr("<none>"));
}

void CAssetEditWidget::ChangeValue(CEditAsset *asset)
{
	SetValue(asset);

	emit ValueChanged(asset);
	if(callback)
		callback->ValueChanged(asset);
}

CEditAsset *CAssetEditWidget::GetValue(void)
{
	return asset;
}


void CAssetEditWidget::SelectAsset(void)
{
	CSelectAssetDialog *dialog = new CSelectAssetDialog(this, main_win);
	main_win->setEnabled(false);
	dialog->show();
}
