
#include "allincludes.h"

void CSceneProject::ExportTowerEngineScene(string file)
{
	xmlDocPtr doc;
	xmlNodePtr root, assets_node, objects_node, scene_node, cur;

	doc = xmlNewDoc((const xmlChar *)"1.0");
	root = xmlNewNode(0, (const xmlChar *)"tscene");
	xmlDocSetRootElement(doc, root);

	assets_node = xmlNewNode(0, (const xmlChar *)"assets");
	xmlAddChild(root, assets_node);

	CEditAsset *asset;

	for(vector<CEditAsset *>::iterator mi=assets.begin(); mi!=assets.end(); mi++)
	{
		asset = *mi;

		cur = asset->CreateSceneExportXMLNode();
		xmlAddChild(assets_node, cur);

		/*switch(asset->GetType())
		{
			case EDIT_ASSET_TYPE_MESH:
				cur = xmlNewNode(0, (const xmlChar *)"mesh");
				xmlSetProp(cur, (const xmlChar *)"name", (const xmlChar *)mesh->GetName().data());
				xmlNodeSetContent(cur, (const xmlChar *)mesh->GetData().data());
				xmlAddChild(assets_node, cur);
				break;
			case EDIT_ASSET_TYPE_CUBE_MAP:
				cur = xmlNewNode(0, (const xmlChar *)"cubemap");
				xmlSetProp(cur, (const xmlChar *)"name", (const xmlChar *)cubemap->GetName().data());
				xmlSetProp(cur, (const xmlChar *)"filename_ext", (const xmlChar *)cubemap->GetFileExtension().data());
				xmlNodeSetContent(cur, (const xmlChar *)cubemap->GetData().toBase64().data());
				xmlAddChild(assets_node, cur);
				break;
		}*/
	}


	objects_node = xmlNewNode(0, (const xmlChar *)"objects");
	xmlAddChild(root, objects_node);

	CEditObject *object;

	for(vector<CEditObject *>::iterator oi=objects.begin(); oi!=objects.end(); oi++)
	{
		object = *oi;

		cur = object->CreateSceneExportXMLNode();
		xmlAddChild(objects_node, cur);

		/*switch(object->GetType())
		{
			case EDIT_OBJECT_TYPE_MESH:
				cur = xmlNewNode(0, (const xmlChar *)"mesh");
				xmlSetProp(cur, (const xmlChar *)"name", (const xmlChar *)object->GetName().data());
				if(mesh_object->GetMeshAsset())
					xmlSetProp(cur, (const xmlChar *)"mesh", (const xmlChar *)mesh_object->GetMeshAsset()->GetName().data());
				xmlSetProp(cur, (const xmlChar *)"x", (const xmlChar *)ftoa(mesh_object->GetTransform().GetPosition().x));
				xmlSetProp(cur, (const xmlChar *)"y", (const xmlChar *)ftoa(mesh_object->GetTransform().GetPosition().y));
				xmlSetProp(cur, (const xmlChar *)"z", (const xmlChar *)ftoa(mesh_object->GetTransform().GetPosition().z));
				xmlAddChild(objects_node, cur);
				break;
			case EDIT_OBJECT_TYPE_DIRECTIONAL_LIGHT:
				cur = xmlNewNode(0, (const xmlChar *)"dir_light");
				xmlSetProp(cur, (const xmlChar *)"name", (const xmlChar *)object->GetName().data());
				xmlSetProp(cur, (const xmlChar *)"x", (const xmlChar *)ftoa(dir_light_object->GetDirection().x));
				xmlSetProp(cur, (const xmlChar *)"y", (const xmlChar *)ftoa(dir_light_object->GetDirection().y));
				xmlSetProp(cur, (const xmlChar *)"z", (const xmlChar *)ftoa(dir_light_object->GetDirection().z));
				xmlSetProp(cur, (const xmlChar *)"r", (const xmlChar *)ftoa(dir_light_object->GetColor().x));
				xmlSetProp(cur, (const xmlChar *)"g", (const xmlChar *)ftoa(dir_light_object->GetColor().y));
				xmlSetProp(cur, (const xmlChar *)"b", (const xmlChar *)ftoa(dir_light_object->GetColor().z));
				xmlAddChild(objects_node, cur);
				break;
			case EDIT_OBJECT_TYPE_POINT_LIGHT:
				cur = xmlNewNode(0, (const xmlChar *)"point_light");
				xmlSetProp(cur, (const xmlChar *)"name", (const xmlChar *)object->GetName().data());
				xmlSetProp(cur, (const xmlChar *)"x", (const xmlChar *)ftoa(point_light_object->GetPosition().x));
				xmlSetProp(cur, (const xmlChar *)"y", (const xmlChar *)ftoa(point_light_object->GetPosition().y));
				xmlSetProp(cur, (const xmlChar *)"z", (const xmlChar *)ftoa(point_light_object->GetPosition().z));
				xmlSetProp(cur, (const xmlChar *)"r", (const xmlChar *)ftoa(point_light_object->GetColor().x));
				xmlSetProp(cur, (const xmlChar *)"g", (const xmlChar *)ftoa(point_light_object->GetColor().y));
				xmlSetProp(cur, (const xmlChar *)"b", (const xmlChar *)ftoa(point_light_object->GetColor().z));
				xmlSetProp(cur, (const xmlChar *)"dist", (const xmlChar *)ftoa(point_light_object->GetDistance()));
				xmlAddChild(objects_node, cur);
				break;
		}*/
	}

	scene_node = xmlNewNode(0, (const xmlChar *)"scene");
	if(CCubeMapEditAsset *cubemap = scene->GetSkyCubeMap())
		xmlSetProp(scene_node, (const xmlChar *)"sky_cubemap", (const xmlChar *)cubemap->GetName().data());
	xmlAddChild(root, scene_node);


	xmlChar *buf;
	int buffer_size;

	xmlDocDumpFormatMemory(doc, &buf, &buffer_size, 1);
	xmlSaveFormatFile(file.data(), doc, 1);
	xmlFree(buf);
	xmlFreeDoc(doc);
}
