﻿#include "allincludes.h"
#include "renderwidget.h"

#include <QFileDialog>

CMainWindow::CMainWindow(CWorldEditor *world_editor, QWidget *parent) : QMainWindow(parent), Ui::MainWindow()
{
	setupUi(this);

	this->world_editor = world_editor;

	tabifyDockWidget(OutlineDockWidget, AssetsDockWidget);
	OutlineDockWidget->raise();

	render_widget = new CRenderWidget(world_editor, this);
	PreviewLayout->addWidget(render_widget);
	render_widget->setFocusPolicy(Qt::StrongFocus);

	OutlineTreeWidget->setHeaderLabel("Name");

	addAction(AddMeshObjectAction);
	addAction(AddDirectionalLightObjectAction);
	addAction(AddPointLightObjectAction);

	connect(AddMeshObjectAction, SIGNAL(triggered()), this, SLOT(AddMeshObject()));
	connect(AddDirectionalLightObjectAction, SIGNAL(triggered()), this, SLOT(AddDirectionalLight()));
	connect(AddPointLightObjectAction, SIGNAL(triggered()), SLOT(AddPointLight()));
	connect(OutlineTreeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(SelectOutlineItem()));

	addAction(DeleteSelectionAction);
	connect(DeleteSelectionAction, SIGNAL(triggered()), this, SLOT(DeleteSelection()));


	connect(AddMeshAssetAction, SIGNAL(triggered()), this, SLOT(CreateMeshAsset()));
	connect(AddCubeMapAssetAction, SIGNAL(triggered()), this, SLOT(CreateCubeMapAsset()));

	connect(InsertMeshToWorldButton, SIGNAL(clicked()), this, SLOT(InsertMeshToWorld()));

	connect(AssetsListWidget, SIGNAL(itemSelectionChanged()), this, SLOT(SelectAssetListItem()));


	addAction(SaveAction);
	addAction(SaveAsAction);
	connect(SaveAction, SIGNAL(triggered()), this, SLOT(Save()));
	connect(SaveAsAction, SIGNAL(triggered()), this, SLOT(SaveAs()));

	connect(ExportTowerEngineSceneAction, SIGNAL(triggered()), this, SLOT(ExportTowerEngineScene()));

	addAction(OpenAction);
	connect(OpenAction, SIGNAL(triggered()), this, SLOT(OpenProject()));

	connect(ViewSettingsAction, SIGNAL(triggered()), this, SLOT(OpenViewSettings()));

	addAction(OutlineWidgetAction);
	connect(OutlineWidgetAction, SIGNAL(toggled(bool)), this, SLOT(SetDockWidgetsVisibilityFromActions()));
	addAction(AssetsWidgetAction);
	connect(AssetsWidgetAction, SIGNAL(toggled(bool)), this, SLOT(SetDockWidgetsVisibilityFromActions()));
	addAction(PropertiesWidgetAction);
	connect(PropertiesWidgetAction, SIGNAL(toggled(bool)), this, SLOT(SetDockWidgetsVisibilityFromActions()));

	connect(OutlineDockWidget, SIGNAL(destroyed()), this, SLOT(SetDockWidgetsVisibilityActions()));
	connect(AssetsDockWidget, SIGNAL(destroyed()), this, SLOT(SetDockWidgetsVisibilityActions()));
	connect(PropertiesDockWidget, SIGNAL(destroyed()), this, SLOT(SetDockWidgetsVisibilityActions()));
}

CMainWindow::~CMainWindow(void)
{
}

void CMainWindow::AddMeshObject(void)
{
	CMeshEditObject *object = new CMeshEditObject(world_editor->GetSceneProject());
	object->SetName(string("mesh"));
	NewObject(object);
}

void CMainWindow::AddDirectionalLight(void)
{
	CDirectionalLightEditObject *object = new CDirectionalLightEditObject(world_editor->GetSceneProject(), Vec(1.0, 1.0, -1.0), Vec(1.0, 1.0, 1.0));
	object->SetName(string("dir_light"));
	NewObject(object);
}

void CMainWindow::AddPointLight(void)
{
	CPointLightEditObject *object = new CPointLightEditObject(world_editor->GetSceneProject(), Vec(0.0, 0.0, 0.0), Vec(0.7, 0.7, 0.7), 5.0);
	object->SetName(string("point_light"));
	NewObject(object);
}

void CMainWindow::InsertMeshToWorld(void)
{
	if(AssetsListWidget->selectedItems().count() != 1)
		return;

	CEditAsset *asset = world_editor->GetSceneProject()->GetAsset(AssetsListWidget->selectedItems().at(0)->data(Qt::UserRole).toInt());

	if(asset->GetType() != EDIT_ASSET_TYPE_MESH)
		return;

	CMeshEditAsset *mesh = (CMeshEditAsset *)asset;
	CMeshEditObject *object = new CMeshEditObject(world_editor->GetSceneProject(), mesh);
	object->SetName(mesh->GetName());
	NewObject(object);
}

void CMainWindow::NewObject(CEditObject *object)
{
	world_editor->GetSceneProject()->AddObject(object);
	world_editor->GetEditSpace()->AddObject(object);
	InitObjectSelection(object);
	UpdateOutline();
}

void CMainWindow::NewAsset(CEditAsset *asset)
{
	world_editor->GetSceneProject()->AddAsset(asset);
	InitAssetSelection(asset);
	UpdateAssetsList();
}

QIcon *CMainWindow::GetObjectTypeIcon(int type)
{
	switch(type)
	{
		case EDIT_OBJECT_TYPE_MESH:
			return new QIcon(":/object_icons/mesh");
		case EDIT_OBJECT_TYPE_DIRECTIONAL_LIGHT:
			return new QIcon(":/object_icons/dir_light");
		case EDIT_OBJECT_TYPE_POINT_LIGHT:
			return new QIcon(":/object_icons/point_light");
		default:
			return new QIcon(":/object_icons/default");
	}

}

QIcon *CMainWindow::GetAssetTypeIcon(int type)
{
	switch(type)
	{
		case EDIT_ASSET_TYPE_MESH:
			return new QIcon(":/asset_icons/mesh");
		case EDIT_ASSET_TYPE_CUBE_MAP:
			return new QIcon(":/asset_icons/cubemap");
		default:
			return new QIcon(":/asset_icons/default");
	}
}

void CMainWindow::ClearLayout(QLayout *layout)
{
	while(!layout->isEmpty())
		delete layout->itemAt(0)->widget();
}

void CMainWindow::AddPropertiesWidget(QWidget *widget)
{
	PropertiesLayout->addWidget(widget);
}

void CMainWindow::ClearPropertiesWidgets(void)
{
	ClearLayout(PropertiesLayout);
}

QFrame *CMainWindow::CreateLine(QWidget *parent)
{
	QFrame *l = new QFrame(parent);
	l->setObjectName(tr("line"));
	l->setFrameShape(QFrame::HLine);
	l->setFrameShadow(QFrame::Sunken);
	return l;
}

void CMainWindow::ClearSelection(void)
{
	world_editor->ClearSelection();
}

void CMainWindow::SelectionChanged(void)
{
	ClearPropertiesWidgets();

	CSelection *selection = world_editor->GetSelection();

	selection->Lock();

	switch(selection->GetType())
	{
		case SELECTION_TYPE_SINGLE_OBJECT:
			{
				CEditObject *object = selection->GetSingleObject();

				QWidget *w;
				for(int i=0; i<object->GetPropertiesCount(); i++)
				{
					w = object->GetProperty(i)->CreateEditWidget(this);
					if(w)
						AddPropertiesWidget(w);

					if(i==0) // Create a line after first EditWidget, which is the one for the object name
						AddPropertiesWidget(CreateLine(this));
				}
			}
			break;
		case SELECTION_TYPE_SINGLE_ASSET:
			{
				CEditAsset *asset = selection->GetSingleAsset();

				CProperty *p;
				for(int i=0; i<asset->GetPropertiesCount(); i++)
				{
					p = asset->GetProperty(i);
					AddPropertiesWidget(p->CreateEditWidget(this));

					if(i==0)
						AddPropertiesWidget(CreateLine(this));
				}
			}
			break;
		case SELECTION_TYPE_SCENE:
			{
				CEditScene *scene = world_editor->GetSceneProject()->GetScene();

				CProperty *p;
				for(int i=0; i<scene->GetPropertiesCount(); i++)
				{
					p = scene->GetProperty(i);
					AddPropertiesWidget(p->CreateEditWidget(this));
				}
			}
			break;
	}

	UpdateEditAxes();

	selection->Unlock();
}

void CMainWindow::InitObjectSelection(CEditObject *object)
{
	world_editor->SelectObject(object);
}

void CMainWindow::InitAssetSelection(CEditAsset *asset)
{
	world_editor->SelectAsset(asset);
}

void CMainWindow::SelectScene(void)
{
	world_editor->SelectScene();
}

void CMainWindow::SelectOutlineItem(void)
{
	if(world_editor->GetSelection()->GetLocked())
		return;

	if(OutlineTreeWidget->selectedItems().size() != 1)
	{
		ClearSelection();
		return;
	}

	COutlineItemData item_data = OutlineTreeWidget->selectedItems().at(0)->data(0, Qt::UserRole).value<COutlineItemData>();
	CEditObject *object;

	switch(item_data.type)
	{
		case COutlineItemData::OBJECTS_TREE_ITEM_OBJECT:
			object = (CEditObject *)item_data.object;
			InitObjectSelection(object);
			break;
		case COutlineItemData::OBJECTS_TREE_ITEM_SCENE:
			SelectScene();
			break;
		default:
			ClearSelection();
			break;
	}

	world_editor->GetSelection()->Lock();
	AssetsListWidget->clearSelection();
	world_editor->GetSelection()->Unlock();
}

void CMainWindow::SelectAssetListItem(void)
{
	if(world_editor->GetSelection()->GetLocked())
		return;

	if(AssetsListWidget->selectedItems().size() != 1)
	{
		ClearSelection();
		return;
	}

	int item_data = AssetsListWidget->selectedItems().at(0)->data(Qt::UserRole).toInt();
	CEditAsset *asset = world_editor->GetSceneProject()->GetAsset(item_data);

	if(!asset)
	{
		ClearSelection();
		return;
	}

	InitAssetSelection(asset);

	world_editor->GetSelection()->Lock();
	OutlineTreeWidget->clearSelection();
	world_editor->GetSelection()->Unlock();
}

void CMainWindow::UpdateOutline(void)
{
	world_editor->GetSelection()->Lock();

	OutlineTreeWidget->clear();

	QTreeWidgetItem *scene_item = new QTreeWidgetItem(OutlineTreeWidget);
	scene_item->setText(0, tr("Scene"));
	scene_item->setIcon(0, QIcon(":/object_icons/scene"));
	scene_item->setData(0, Qt::UserRole, QVariant::fromValue(COutlineItemData(COutlineItemData::OBJECTS_TREE_ITEM_SCENE)));

	int i;
	CSceneProject *world = world_editor->GetSceneProject();
	CEditObject *object;
	QTreeWidgetItem *item;

	for(i=0; i<world->GetObjectsCount(); i++)
	{
		object = world->GetObject(i);
		item = new QTreeWidgetItem(scene_item);
		item->setText(0, QString(object->GetName().data()));
		item->setIcon(0, *GetObjectTypeIcon(object->GetType()));
		item->setData(0, Qt::UserRole, QVariant::fromValue(COutlineItemData(COutlineItemData::OBJECTS_TREE_ITEM_OBJECT, object)));

		if(object == world_editor->GetSelection()->GetSingleObject())
			item->setSelected(true);
	}

	scene_item->setExpanded(true);

	world_editor->GetSelection()->Unlock();
}

void CMainWindow::UpdateAssetsList(void)
{
	world_editor->GetSelection()->Lock();

	AssetsListWidget->clear();

	int i;
	CSceneProject *world = world_editor->GetSceneProject();
	CMeshEditAsset *asset;
	QListWidgetItem *item;

	for(i=0; i<world->GetAssetsCount(); i++)
	{
		asset = (CMeshEditAsset *)world->GetAsset(i);
		item = new QListWidgetItem(AssetsListWidget);
		item->setText(QString(asset->GetName().data()));
		item->setIcon(*GetAssetTypeIcon(asset->GetType()));
		item->setData(Qt::UserRole, QVariant(i)); // TODO: save pointer instead
		AssetsListWidget->addItem(item);

		if(world_editor->GetSelection()->GetSingleAsset() == asset)
			item->setSelected(true);
	}

	world_editor->GetSelection()->Unlock();
}

void CMainWindow::UpdateEditAxes(void)
{
	CEditSpace *space = world_editor->GetEditSpace();

	space->SetMoveEditAxes(false, Vec(0.0, 0.0, 0.0));

	if(world_editor->GetSelection()->GetType() != SELECTION_TYPE_SINGLE_OBJECT)
		return;

	CEditObject *object = world_editor->GetSelection()->GetSingleObject();

	switch(object->GetType())
	{
		case EDIT_OBJECT_TYPE_MESH:
			space->SetMoveEditAxes(true, ((CMeshEditObject *)object)->GetTransform().GetPosition());
			break;
		case EDIT_OBJECT_TYPE_POINT_LIGHT:
			space->SetMoveEditAxes(true, ((CPointLightEditObject *)object)->GetPosition());
			break;
	}
}

void CMainWindow::ObjectNameChanged(CEditObject *object)
{
	UpdateOutline();
}

void CMainWindow::ObjectTransformChanged(CEditObject *object)
{
	if(world_editor->GetSelection()->GetSingleObject() == object)
	{
		UpdateEditAxes();
	}
}

void CMainWindow::AssetNameChanged(CEditAsset *asset)
{
	UpdateAssetsList();
}

void CMainWindow::DeleteSelection(void)
{
	if(world_editor->GetSelection()->GetLocked() || world_editor->GetSelection()->GetType() != SELECTION_TYPE_SINGLE_OBJECT)
		return;

	CEditObject *object = world_editor->GetSelection()->GetSingleObject();

	world_editor->GetSceneProject()->RemoveObject(object);
	delete object;
	ClearSelection();
	UpdateOutline();
	UpdateEditAxes();
}

void CMainWindow::InitSceneProjectChange(void)
{
	ClearSelection();
}

void CMainWindow::UpdateSceneInterface(void)
{
	UpdateAssetsList();
	UpdateOutline();
}

void CMainWindow::CreateMeshAsset(void)
{
	CMeshEditAsset *asset = new CMeshEditAsset(world_editor->GetSceneProject(), string("mesh"), string(""));
	NewAsset(asset);
}

void CMainWindow::CreateCubeMapAsset(void)
{
	CCubeMapEditAsset *asset = new CCubeMapEditAsset(world_editor->GetSceneProject(), string("cube_map"), string());
	NewAsset(asset);
}

void CMainWindow::SetDockWidgetsVisibilityFromActions(void)
{
	OutlineDockWidget->setVisible(OutlineWidgetAction->isChecked());
	AssetsDockWidget->setVisible(AssetsWidgetAction->isChecked());
	PropertiesDockWidget->setVisible(PropertiesWidgetAction->isChecked());
}

void CMainWindow::SetDockWidgetsVisibilityActions(void)
{
	OutlineWidgetAction->setChecked(OutlineDockWidget->isVisible());
	AssetsWidgetAction->setChecked(AssetsDockWidget->isVisible());
	PropertiesWidgetAction->setChecked(PropertiesDockWidget->isVisible());
}

void CMainWindow::UpdateTitle(void)
{
	CSceneProject *p = world_editor->GetSceneProject();

	QString title = tr("TowerEngine World Editor");

	string f = p->GetFilename();
	if(f.size() > 0)
		title = title + tr(" (") + QString::fromStdString(f) + tr(")");

	setWindowTitle(title);
}

#include <QMessageBox>

void CMainWindow::Save(void)
{
	string f = world_editor->GetSceneProject()->GetFilename();

	if(f.size() == 0)
		SaveAs();
	else
	{
		if(!world_editor->SaveSceneProject(f))
			QMessageBox::critical(this, tr("Save Project"), tr("Failed to save project to \"") + QString::fromStdString(f) + tr("\""));
		UpdateTitle();
	}
}

void CMainWindow::SaveAs(void)
{
	QFileDialog dialog(this);

	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setNameFilter(tr("Tower Engine Scene Project (*.tsp)"));
	dialog.setViewMode(QFileDialog::List);
	dialog.setWindowTitle("Save Project (.tsp)");

	if(dialog.exec())
	{
		string f = dialog.selectedFiles().at(0).toStdString();
		if(!world_editor->SaveSceneProject(f))
			QMessageBox::critical(this, tr("Save Project"), tr("Failed to save project to \"") + QString::fromStdString(f) + tr("\""));
	}

	UpdateTitle();
}

void CMainWindow::ExportTowerEngineScene(void)
{
	QFileDialog dialog(this);

	dialog.setAcceptMode(QFileDialog::AcceptSave);
	dialog.setFileMode(QFileDialog::AnyFile);
	dialog.setNameFilter(tr("Tower Engine Scene (*.tes)"));
	dialog.setViewMode(QFileDialog::List);
	dialog.setWindowTitle("Export Tower Engine Scene (.tes)");

	if(dialog.exec())
		world_editor->ExportTowerEngineScene(dialog.selectedFiles().at(0).toStdString());
}

void CMainWindow::OpenProject(void)
{
	QFileDialog dialog(this);

	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setNameFilter(tr("Tower Engine Scene Project (*.tsp)"));
	dialog.setViewMode(QFileDialog::List);
	dialog.setWindowTitle("Open Project (.tsp)");

	if(dialog.exec())
		world_editor->LoadSceneProject(dialog.selectedFiles().at(0).toStdString());

	UpdateTitle();
}

void CMainWindow::OpenViewSettings(void)
{
	setEnabled(false);
	CViewSettingsDialog *dialog = new CViewSettingsDialog(this, world_editor->GetEditSpace());
	dialog->show();
}
