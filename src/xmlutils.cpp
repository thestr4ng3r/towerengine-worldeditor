
#include "xmlutils.h"



xmlNodePtr CreateFloatXMLNode(const xmlChar *name, float v, const xmlChar *p)
{
	xmlNodePtr n = xmlNewNode(0, name);
	xmlSetProp(n, p, (const xmlChar *)ftoa(v));
	return n;
}

xmlNodePtr CreateVectorXMLNode(const xmlChar *name, tVector v, const xmlChar *x_p, const xmlChar *y_p, const xmlChar *z_p)
{
	xmlNodePtr n = xmlNewNode(0, name);
	xmlSetProp(n, x_p, (const xmlChar *)ftoa(v.x));
	xmlSetProp(n, y_p, (const xmlChar *)ftoa(v.y));
	xmlSetProp(n, z_p, (const xmlChar *)ftoa(v.z));
	return n;
}

xmlNodePtr CreateEditTransformXMLNode(const xmlChar *name, CEditTransform trans)
{
	xmlNodePtr n;
	xmlNodePtr child;

	n = xmlNewNode(0, name);

	child = CreateVectorXMLNode((const xmlChar *)"position", trans.GetPosition());
	xmlAddChild(n, child);

	child = CreateVectorXMLNode((const xmlChar *)"rotation", trans.GetRotation());
	xmlAddChild(n, child);

	child = CreateVectorXMLNode((const xmlChar *)"scale", trans.GetScale());
	xmlAddChild(n, child);

	return n;
}


xmlNodePtr CreateTransformXMLNode(const xmlChar *name, tTransform trans)
{
	xmlNodePtr n;
	xmlNodePtr child;

	n = xmlNewNode(0, name);

	child = CreateVectorXMLNode((const xmlChar *)"position", trans.GetPosition());
	xmlAddChild(n, child);

	tMatrix3 basis = trans.GetBasis();

	child = CreateVectorXMLNode((const xmlChar *)"basis_x", basis.GetX());
	xmlAddChild(n, child);
	child = CreateVectorXMLNode((const xmlChar *)"basis_y", basis.GetY());
	xmlAddChild(n, child);
	child = CreateVectorXMLNode((const xmlChar *)"basis_z", basis.GetZ());
	xmlAddChild(n, child);

	return n;
}


float ParseFloatXMLNode(xmlNodePtr node, const xmlChar *p)
{
	float r;

	xmlChar *temp;

	if(temp = xmlGetProp(node, p))
		r = atof((const char *)temp);

	return r;
}

tVector ParseVectorXMLNode(xmlNodePtr node, const xmlChar *x_p, const xmlChar *y_p, const xmlChar *z_p)
{
	tVector r = Vec(0.0, 0.0, 0.0);

	xmlChar *temp;

	if(temp = xmlGetProp(node, x_p))
		r.x = atof((const char *)temp);

	if(temp = xmlGetProp(node, y_p))
		r.y = atof((const char *)temp);

	if(temp = xmlGetProp(node, z_p))
		r.z = atof((const char *)temp);

	return r;
}

CEditTransform ParseEditTransformXMLNode(xmlNodePtr node)
{
	xmlNodePtr child;
	CEditTransform trans;

	for(child=node->children; child; child=child->next)
	{
		if(xmlStrEqual(child->name, tcxc("position")))
			trans.SetPosition(ParseVectorXMLNode(child));
		else if(xmlStrEqual(child->name, tcxc("rotation")))
			trans.SetRotation(ParseVectorXMLNode(child));
		else if(xmlStrEqual(child->name, tcxc("scale")))
			trans.SetScale(ParseVectorXMLNode(child));
	}

	return trans;
}
