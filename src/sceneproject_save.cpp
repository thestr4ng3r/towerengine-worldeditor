
#include "allincludes.h"


bool CSceneProject::SaveSceneProject(string file)
{
	xmlDocPtr doc;
	xmlNodePtr root, assets_node, objects_node, edit_camera_node, scene_node, cur;

	doc = xmlNewDoc((const xmlChar *)"1.0");
	root = xmlNewNode(0, (const xmlChar *)"tsceneproject");
	xmlDocSetRootElement(doc, root);


	edit_camera_node = xmlNewNode(0, (const xmlChar *)"edit_camera");
	xmlAddChild(root, edit_camera_node);

	tVector cam_pos = world_editor->GetEditSpace()->GetEditCameraPosition();
	tVector2 cam_rot = world_editor->GetEditSpace()->GetEditCameraRotation();

	xmlSetProp(edit_camera_node, (const xmlChar *)"x", (const xmlChar *)ftoa(cam_pos.x));
	xmlSetProp(edit_camera_node, (const xmlChar *)"y", (const xmlChar *)ftoa(cam_pos.y));
	xmlSetProp(edit_camera_node, (const xmlChar *)"z", (const xmlChar *)ftoa(cam_pos.z));

	xmlSetProp(edit_camera_node, (const xmlChar *)"rx", (const xmlChar *)ftoa(cam_rot.x));
	xmlSetProp(edit_camera_node, (const xmlChar *)"ry", (const xmlChar *)ftoa(cam_rot.y));


	assets_node = xmlNewNode(0, (const xmlChar *)"assets");
	xmlAddChild(root, assets_node);

	CEditAsset *asset;

	for(vector<CEditAsset *>::iterator mi=assets.begin(); mi!=assets.end(); mi++)
	{
		asset = *mi;

		cur = asset->CreateSceneProjectXMLNode();
		xmlAddChild(assets_node, cur);
	}

	objects_node = xmlNewNode(0, (const xmlChar *)"objects");
	xmlAddChild(root, objects_node);

	CEditObject *object;

	for(vector<CEditObject *>::iterator oi=objects.begin(); oi!=objects.end(); oi++)
	{
		object = *oi;

		cur = object->CreateSceneProjectXMLNode();
		xmlAddChild(objects_node, cur);
	}


	scene_node = xmlNewNode(0, (const xmlChar *)"scene");
	if(CCubeMapEditAsset *cubemap = scene->GetSkyCubeMap())
		xmlSetProp(scene_node, (const xmlChar *)"sky_cubemap", (const xmlChar *)cubemap->GetName().data());
	xmlAddChild(root, scene_node);



	xmlChar *buf;
	int buffer_size;

	xmlDocDumpFormatMemory(doc, &buf, &buffer_size, 1);
	int ret = xmlSaveFormatFile(file.data(), doc, 1);
	xmlFree(buf);
	xmlFreeDoc(doc);

	if(ret != -1)
	{
		this->filename = file;
		return true;
	}
	else
		return false;
}
