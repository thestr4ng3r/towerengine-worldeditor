#include "allincludes.h"

CGridObject::CGridObject(tVector x, tVector y, int size, tVector color) : tObject()
{
	this->x = x;
	this->y = y;
	this->size = size;
	this->color = color;

	vao = new tVAO();

	vbo = new tVBO<float>(3, vao, size * 4);

	tVector v;
	int xi, yi;
	float *vbo_data = vbo->GetData();

	for(xi=0; xi<size+1; xi++)
	{
		v =	x * (-((float)size / 2.0) + (float)xi) +
			y * -((float)size / 2.0);
		memcpy(vbo_data + xi * 3, v.v, sizeof(float) * 3);


		v =	x * (-((float)size / 2.0) + (float)xi) +
			y * ((float)size / 2.0);
		memcpy(vbo_data + ((size + 1) + xi) * 3, v.v, sizeof(float) * 3);
	}

	for(yi=1; yi<size; yi++)
	{
		v =	x * -((float)size / 2.0) +
			y * (-((float)size / 2.0) + yi);
		memcpy(vbo_data + ((size + 1) * 2 + (yi-1)) * 3, v.v, sizeof(float) * 3);


		v =	x * ((float)size / 2.0) +
			y * (-(float)size / 2.0 + yi);
		memcpy(vbo_data + ((size + 1) * 2 + (size - 1) + (yi-1)) * 3, v.v, sizeof(float) * 3);
	}

	vbo->AssignData();

	ibo = new tIBO(vao, (size + 1) * 4);

	GLuint *ibo_data = ibo->GetData();

	for(xi=0; xi<size+1; xi++)
	{
		ibo_data[xi*2] = xi;
		ibo_data[xi*2+1] = (size + 1) + xi;
	}

	ibo_data[(size+1)*2] = 0;
	ibo_data[(size+1)*2+1] = size;


	ibo_data[(size+1)*2+size*2] = size+1;
	ibo_data[(size+1)*2+size*2+1] = size*2+1;

	for(yi=1; yi<size; yi++)
	{
		ibo_data[(size+1)*2+yi*2] = (size + 1) * 2 + yi - 1;
		ibo_data[(size+1)*2+yi*2+1] = ((size + 1) * 2 + (size - 1) + yi - 1);
	}

	ibo->AssignData();
}

CGridObject::~CGridObject(void)
{

}

void CGridObject::ForwardPass(void)
{
	glEnable(GL_DEPTH_TEST);
	glLineWidth(1.0);

	tEngine::GetColorShader()->Bind();
	tEngine::GetColorShader()->SetDiffuseColor(color);
	tEngine::GetColorShader()->SetTransformation(tEngine::identity_matrix4);

	vbo->SetAttribute(tFaceShader::vertex_attribute, GL_FLOAT);

	ibo->Draw(GL_LINES);
}

tBoundingBox CGridObject::GetBoundingBox(void)
{
	tBoundingBox b;
	b.SetBounds(	-x * ((float)size / 2.0) - y * ((float)size / 2.0),
					 x * ((float)size / 2.0) + y * ((float)size / 2.0));
	return b;
}
