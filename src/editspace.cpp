
#include "editobject.h"
#include "editspace.h"
#include "cubemapeditasset.h"

CEditSpace::CEditSpace(CWorldEditor *world_editor)
{
	this->world_editor = world_editor;

	world = 0;

	scene_sky_box = 0;

	x_move_edit_axis = 0;
	y_move_edit_axis = 0;
	z_move_edit_axis = 0;

	physics.broadphase = 0;
	physics.collision_configuration = 0;
	physics.collision_dispatcher = 0;
	physics.solver = 0;
	physics.world = 0;

	drag_moving = false;
}

CEditSpace::~CEditSpace(void)
{
	safe_delete(world);

	safe_delete(x_move_edit_axis);
	safe_delete(y_move_edit_axis);
	safe_delete(z_move_edit_axis);

	safe_delete(physics.world);
	safe_delete(physics.solver);
	safe_delete(physics.collision_dispatcher);
	safe_delete(physics.collision_configuration);
	safe_delete(physics.broadphase);
}

void CEditSpace::RotateCamera(float x, float y)
{
	cam_rot += Vec(x, y);
	cam_rot.y = max(min(cam_rot.y, (float)(M_PI / 2.0 - 0.1)), (float)(-M_PI / 2.0 + 0.1));
}

void CEditSpace::MoveCamera(float x, float z, bool fast)
{
	cam_dir = Vec(cos(cam_rot.x) * cos(cam_rot.y), sin(cam_rot.y), sin(cam_rot.x) * cos(cam_rot.y));

	tVector side = Cross(Vec(0.0, 1.0, 0.0), cam_dir);
	side.Normalize();

	cam_pos += (cam_dir * z + side * x) * (fast ? cam_speed_fast : cam_speed_normal);
}

void CEditSpace::SetEditCamera(tVector pos, tVector2 rot)
{
	cam_pos = pos;
	cam_rot = rot;
}


void CEditSpace::Init(int width, int height)
{
	tEngine::Init();

	world = new tWorld();

	physics.broadphase = new btDbvtBroadphase();
	physics.collision_configuration = new btDefaultCollisionConfiguration();
	physics.collision_dispatcher = new btCollisionDispatcher(physics.collision_configuration);
	physics.solver = new btSequentialImpulseConstraintSolver();
	physics.world = new btDiscreteDynamicsWorld(physics.collision_dispatcher, physics.broadphase, physics.solver, physics.collision_configuration);
	physics.world->getDispatchInfo().m_allowedCcdPenetration=0.0001f;

	world->GetCamera()->SetClip(0.1, 1000.0);
	SetViewSettings(0.15, 0.5, 60.0);

	renderer = new tRenderer(width, height, world);

	cam_pos = Vec(-3.0, 3.0, 3.0);
	cam_rot = Vec(-0.74, -0.62);

	SetScreenSize(width, height);



	coordinate_system_object = new tCoordinateSystemObject(true);
	world->AddObject(coordinate_system_object);

	grid_object = new CGridObject(Vec(1.0, 0.0, 0.0), Vec(0.0, 0.0, -1.0), 32);
	world->AddObject(grid_object);

	x_move_edit_axis = new CEditAxisObject(this, Vec(1.0, 0.0, 0.0), Vec(1.0, 0.0, 0.0) * 0.1);
	y_move_edit_axis = new CEditAxisObject(this, Vec(0.0, 1.0, 0.0), Vec(0.0, 1.0, 0.0) * 0.1);
	z_move_edit_axis = new CEditAxisObject(this, Vec(0.0, 0.0, 1.0), Vec(0.0, 0.0, 1.0) * 0.1);

	SetMoveEditAxes(false, Vec(0.0, 0.0, 0.0));
	world->AddObject(x_move_edit_axis);
	world->AddObject(y_move_edit_axis);
	world->AddObject(z_move_edit_axis);


	static const char *sky_fn[6] = {	"sky/xp.png",
										"sky/xn.png",
										"sky/yp.png",
										"sky/yn.png",
										"sky/zp.png",
										"sky/zn.png" };
	default_sky_box = new tSkyBox(LoadGLCubeMap(sky_fn));

	scene_sky_box = 0;

	world->SetSkyBox(default_sky_box);

	drag_moving = false;
}


void CEditSpace::AddObject(CEditObject *object)
{
	if(object_connections.find(object) != object_connections.end())
		return;

	object_connections.insert(std::pair<CEditObject *, CEditObjectSpaceConnection *>(object, object->CreateSpaceConnection(this)));
}

void CEditSpace::RemoveObject(CEditObject *object)
{
	map<CEditObject *, CEditObjectSpaceConnection *>::iterator it = object_connections.find(object);

	if(it != object_connections.end())
	{
		delete it->second;
		object_connections.erase(it);
	}
}

void CEditSpace::SetFromProject(CSceneProject *project)
{
	for(int i=0; i<project->GetObjectsCount(); i++)
		AddObject(project->GetObject(i));

	SetSkyBoxFromEditScene(project->GetScene());
}

void CEditSpace::Clear(void)
{
	map<CEditObject *, CEditObjectSpaceConnection *>::iterator it;
	for(it = object_connections.begin(); it != object_connections.end(); it++)
		delete it->second;

	object_connections.clear();

	world->SetSkyBox(default_sky_box);

	delete scene_sky_box;
	scene_sky_box = 0;
}

void CEditSpace::SetSkyBoxFromEditScene(CEditScene *scene)
{
	delete scene_sky_box;
	scene_sky_box = 0;

	CCubeMapEditAsset *cubemap = scene->GetSkyCubeMap();

	if(cubemap)
	{
		scene_sky_box = new tSkyBox(cubemap->GetCubeMap());
		world->SetSkyBox(scene_sky_box);
	}
	else
	{
		world->SetSkyBox(default_sky_box);
	}
}


void CEditSpace::Render(void)
{
	cam_dir = Vec(cos(cam_rot.x) * cos(cam_rot.y), sin(cam_rot.y), sin(cam_rot.x) * cos(cam_rot.y));

	tVector pos = edit_axes_pos - cam_pos;
	pos.Normalize();
	pos += cam_pos;

	x_move_edit_axis->SetPosition(pos);
	y_move_edit_axis->SetPosition(pos);
	z_move_edit_axis->SetPosition(pos);

	world->GetCamera()->SetPosition(cam_pos);
	world->GetCamera()->SetDirection(cam_dir);

	renderer->Render();


	// overlay...

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glUseProgram(0);

	glColor4f(1.0, 0.0, 0.0, 1.0);
	glLineWidth(1.0);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/*glBegin(GL_LINES);
	glVertex2f(temp1.x, temp1.y);
	glVertex2f(temp2.x, temp2.y);
	glEnd();*/
}

void CEditSpace::SetScreenSize(int width, int height)
{
	this->render_width = width;
	this->render_height = height;

	renderer->ChangeSize(width, height);
}




void CEditSpace::SetViewSettings(float cam_speed_normal, float cam_speed_fast, float cam_angle)
{
	this->cam_speed_normal = cam_speed_normal;
	this->cam_speed_fast = cam_speed_fast;
	this->cam_angle = cam_angle;

	world->GetCamera()->SetAngle(cam_angle);
}



class CEditSpacePickingRayResultCallback : public btCollisionWorld::RayResultCallback
{
	private:
		CEditSpace *space;

		int axis;
		CEditObject *object;

		btScalar closest_object_fraction;

	public:
		CEditSpacePickingRayResultCallback(CEditSpace *space) : btCollisionWorld::RayResultCallback()
		{
			this->space = space;
			axis = -1;
			object = 0;
			closest_object_fraction = 1.0;
		}

		btScalar addSingleResult(btCollisionWorld::LocalRayResult &result, bool normal_in_world_space)
		{
			void *user_pointer = result.m_collisionObject->getUserPointer();

			int found_axis = space->FindEditAxisByPointer(user_pointer);
			if(found_axis != -1) // found an axis
			{
				this->axis = found_axis;
				this->object = 0;
			}
			else if(this->axis == -1 && result.m_hitFraction < closest_object_fraction) // something else and still no axis found..
			{
				CEditObject *object = space->FindObjectByPointer(user_pointer);
				if(object)
				{
					this->object = object;
					this->closest_object_fraction = result.m_hitFraction;
				}
			}

			return m_closestHitFraction;
		}

		int GetAxis(void)				{ return axis; }
		CEditObject *GetObject(void)	{ return object; }
};


void CEditSpace::MouseDown(float x, float y)
{
	tVector cam_pos = world->GetCamera()->GetPosition();
	tVector dir = world->GetCamera()->GetScreenRay(x, y);

	btVector3 start = BtVec(cam_pos);
	btVector3 end = BtVec(cam_pos + dir);

	CEditSpacePickingRayResultCallback ray_callback(this);

	physics.world->updateAabbs();
	physics.world->rayTest(start, end, ray_callback);


	if(ray_callback.GetAxis() != -1)
	{
		drag_moving = true;

		tVector axis_dir = Vec(0.0, 0.0, 0.0);
		axis_dir.v[ray_callback.GetAxis()] += 1.0;

		tVector2 a = world->GetCamera()->GetProjectedPoint(edit_axes_pos);
		tVector2 b = world->GetCamera()->GetProjectedPoint(edit_axes_pos + axis_dir);

		drag_move_vec = b - a;
		drag_move_axis = ray_callback.GetAxis();
	}
	else if(ray_callback.GetObject())
	{
		CEditObject *object = ray_callback.GetObject();
		world_editor->SelectObject(object);
	}
	else
		world_editor->ClearSelection();
}

void CEditSpace::MouseDrag(float dx, float dy)
{
	if(drag_moving)
	{
		tVector v = Vec(0.0, 0.0, 0.0);
		v.v[drag_move_axis] += 1.0;
		tVector2 temp = drag_move_vec;
		temp.Normalize();
		v *= Dot(Vec(dx, dy), temp) / drag_move_vec.Len();

		CEditObject *object = world_editor->GetSelection()->GetSingleObject();
		if(object)
			object->Move(v);
	}
}

void CEditSpace::MouseUp(void)
{
	drag_moving = false;
}


void CEditSpace::SetMoveEditAxes(bool visible, tVector pos)
{
	x_move_edit_axis->SetVisible(visible);
	y_move_edit_axis->SetVisible(visible);
	z_move_edit_axis->SetVisible(visible);
	edit_axes_pos = pos;
}


int CEditSpace::FindEditAxisByPointer(void *p)
{
	if(p == x_move_edit_axis && x_move_edit_axis->GetVisible())
		return 0;
	if(p == y_move_edit_axis && y_move_edit_axis->GetVisible())
		return 1;
	if(p == z_move_edit_axis && z_move_edit_axis->GetVisible())
		return 2;

	return -1;
}

CEditObject *CEditSpace::FindObjectByPointer(void *p)
{
	return world_editor->GetSceneProject()->FindObjectByPointer(p);
}
