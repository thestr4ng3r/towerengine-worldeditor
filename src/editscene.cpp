#include "allincludes.h"

CEditScene::CEditScene(CSceneProject *project)
{
	this->project = project;
	cubemap = 0;

	AddProperty(new CEditSceneCubeMapAssetProperty(this));
}

CEditScene::~CEditScene(void)
{
}

void CEditScene::SetSkyCubeMap(CCubeMapEditAsset *cubemap)
{
	if(cubemap == this->cubemap)
		return;

	this->cubemap = cubemap;
}


CEditSceneCubeMapAssetProperty::CEditSceneCubeMapAssetProperty(CEditScene *scene)
{
	this->scene = scene;
}

QWidget *CEditSceneCubeMapAssetProperty::CreateEditWidget(CMainWindow *main_win)
{
	CAssetEditWidget *w = new CAssetEditWidget(QString("Skybox"), scene->GetSceneProject(), EDIT_ASSET_TYPE_CUBE_MAP, main_win, scene->GetSkyCubeMap());
	w->SetCallback(this);
	return w;
}

xmlNodePtr CEditSceneCubeMapAssetProperty::CreateSceneProjectXMLNode(void)
{
	// TODO
	return 0;
}

xmlNodePtr CEditSceneCubeMapAssetProperty::CreateSceneExportXMLNode(void)
{
	// TODO
	return 0;
}

void CEditSceneCubeMapAssetProperty::ValueChanged(CEditAsset *v)
{
	if(v)
		if(v->GetType() != EDIT_ASSET_TYPE_CUBE_MAP)
			return;

	scene->SetSkyCubeMap((CCubeMapEditAsset *)v);

	// TODO: maybe put somewhere else
	scene->GetSceneProject()->GetWorldEditor()->GetEditSpace()->SetSkyBoxFromEditScene(scene);
}







