#ifndef _EDITAXISOBJECT_H
#define _EDITAXISOBJECT_H

class tObject;
class CBoundingBox;
class tVector;

class CEditAxisObject : public tObject
{
	private:
		CEditSpace *space;

		bool visible;
		tVector pos;
		tVector dir;
		tVector color;
		tVBO<float> *vbo;
		tVAO *vao;
		tIBO *ibo;

		btCollisionShape *shape;
		btRigidBody *rigid_body;
		btMotionState *motion_state;

		void UpdateVBO(void);

	public:
		CEditAxisObject(CEditSpace *space, tVector color = Vec(1.0, 1.0, 1.0), tVector dir = Vec(1.0, 0.0, 0.0));
		~CEditAxisObject(void);

		void ForwardPass(void);

		void SetPosition(tVector pos)	{ this->pos = pos; UpdateVBO(); }
		void SetDirection(tVector dir)	{ this->dir = dir; UpdateVBO(); }
		void SetVisible(bool visible)	{ this->visible = visible; }
		void SetColor(tVector color)	{ this->color = color; }

		bool GetVisible()				{ return visible; }

		tBoundingBox GetBoundingBox(void);

		void AddedToWorld(tWorld *world);
		void RemovedFromWorld(tWorld *world);
};

#endif
