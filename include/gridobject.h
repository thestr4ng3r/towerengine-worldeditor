#ifndef _GRIDOBJECT_H
#define _GRIDOBJECT_H

class tObject;
class tVector;
class CBoundingBox;

class CGridObject : public tObject
{
	private:
		tVector x, y;
		int size;

		tVector color;

		tVAO *vao;
		tVBO<float> *vbo;
		tIBO *ibo;

	public:
		CGridObject(tVector x, tVector y, int size, tVector color = Vec(0.5, 0.5, 0.5));
		~CGridObject(void);

		void ForwardPass(void);

		tBoundingBox GetBoundingBox(void);
};

#endif
