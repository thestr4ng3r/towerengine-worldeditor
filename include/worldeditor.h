#ifndef _WORLDEDITOR_H
#define _WORLDEDITOR_H

#include "mainwindow.h"
#include "sceneproject.h"
#include "selection.h"

class CEditSpace;

class CWorldEditor
{
	private:
		CMainWindow *main_window;

		CSceneProject *scene_project;
		CEditSpace *edit_space;

		CSelection *selection;

	public:
		CWorldEditor();

		CSceneProject *GetSceneProject(void)	{ return scene_project; }
		CEditSpace *GetEditSpace(void)			{ return edit_space; }
		CSelection *GetSelection(void)			{ return selection; }

		void SelectObject(CEditObject *object);
		void SelectAsset(CEditAsset *asset);
		void SelectScene(void);
		void ClearSelection(void);

		void ExportTowerEngineScene(string file);
		bool SaveSceneProject(string file);

		void LoadSceneProject(string file);

		void ObjectNameChanged(CEditObject *object);
		void ObjectTransformChanged(CEditObject *object);
		void AssetNameChanged(CEditAsset *asset);
};

#endif
