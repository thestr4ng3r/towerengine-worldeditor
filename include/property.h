#ifndef _PROPERTY_H
#define _PROPERTY_H

class QWidget;
class CMainWindow;

#include <vector>

#include <libxml/parser.h>


class CProperty
{
	public:
		virtual QWidget *CreateEditWidget(CMainWindow *main_win) =0;
		virtual xmlNodePtr CreateSceneProjectXMLNode(void) =0;
		virtual xmlNodePtr CreateSceneExportXMLNode(void) =0;
};

class CPropertiesContainer
{
	private:
		std::vector<CProperty *> properties;

	protected:
		void AddProperty(CProperty *p)	{ properties.push_back(p); }

	public:
		int GetPropertiesCount(void)	{ return properties.size(); }
		CProperty *GetProperty(int i)	{ return properties.at(i); }
};

#endif
