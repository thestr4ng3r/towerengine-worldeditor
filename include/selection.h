#ifndef _SELECTION_H
#define _SELECTION_H

#define SELECTION_TYPE_EMPTY			0
#define SELECTION_TYPE_SINGLE_OBJECT	1
#define SELECTION_TYPE_SINGLE_ASSET		2
#define SELECTION_TYPE_SCENE			3

class CEditObject;
class CEditAsset;

class CSelection
{
	private:
		union
		{
				CEditObject *single_object;
				CEditAsset *single_asset;
		};

		bool locked;
		int type;

	public:
		CSelection(void);

		void Clear(void);

		void Set(CEditObject *single_object);
		void Set(CEditAsset *single_asset);
		void SetType(int type);

		void Lock(void)			{ locked = true; }
		void Unlock(void)		{ locked = false; }

		CEditObject *GetSingleObject(void)	{ return type == SELECTION_TYPE_SINGLE_OBJECT ? single_object : 0; }
		CEditAsset *GetSingleAsset(void)	{ return type == SELECTION_TYPE_SINGLE_ASSET ? single_asset : 0; }

		int GetType(void)		{ return type; }
		bool GetLocked(void)	{ return locked; }
};

#endif
