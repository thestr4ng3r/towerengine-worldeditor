#ifndef _EDITWORLD_H
#define _EDITWORLD_H

#include <vector>

#include "editscene.h"

class tCoordinateSystemObject;
class CEditAxisObject;
class CWorldEditor;

class CEditAsset;
class CMeshEditAsset;
class CCubeMapEditAsset;

class CSceneProject
{
	private:
		CWorldEditor *world_editor;

		string filename;
		bool changed;


		CEditScene *scene;

		std::vector<CEditObject *> objects;
		std::vector<CEditAsset *> assets;

		bool loaded;

		void InitEmptyScene(void);

		bool LoadFromFile(string file);

		void ParseAssetsNode(xmlNodePtr cur);
		void ParseMeshAssetNode(xmlNodePtr cur);
		void ParseCubeMapAssetNode(xmlNodePtr cur);

		void ParseObjectsNode(xmlNodePtr cur);
		void ParseMeshObjectNode(xmlNodePtr cur);
		void ParseDirectionalLightObjectNode(xmlNodePtr cur);
		void ParsePointLightObjectNode(xmlNodePtr cur);

		void ParseEditCameraNode(xmlNodePtr cur);
		void ParseSceneNode(xmlNodePtr cur);


	public:
		CSceneProject(CWorldEditor *editor);
		CSceneProject(CWorldEditor *editor, string file);
		~CSceneProject(void);

		CWorldEditor *GetWorldEditor(void)	{ return world_editor; }

		bool GetLoaded(void)	{ return loaded; }

		bool GetChanged(void)		{ return changed; }
		void TriggerChanged(void)	{ changed = true; }

		string GetFilename(void)	{ return filename; }

		CEditScene *GetScene(void)	{ return scene; }

		void AddObject(CEditObject *object);
		void RemoveObject(CEditObject *object);

		void AddAsset(CEditAsset *asset);
		void RemoveAsset(CEditAsset *asset);

		int GetObjectsCount(void)		{ return objects.size(); }
		CEditObject *GetObject(int i)	{ return objects.at(i); }
		CEditObject *FindObjectByName(string name, CEditObject *self = 0);

		int GetAssetsCount(void)		{ return assets.size(); }
		CEditAsset *GetAsset(int i)		{ return assets.at(i); }
		CEditAsset *FindAssetByName(string name, CEditAsset *self = 0);

		string GetUniqueObjectName(string name, CEditObject *self = 0);
		string GetUniqueAssetName(string name, CEditAsset *self = 0);


		void AssetContentChanged(CEditAsset *asset);
		void MeshAssetContentChanged(CMeshEditAsset *asset);
		void CubeMapAssetContentChanged(CCubeMapEditAsset *asset);

		void ObjectNameChanged(CEditObject *object);
		void ObjectTransformChanged(CEditObject *object);
		void AssetNameChanged(CEditAsset *asset);

		void ExportTowerEngineScene(string file);
		bool SaveSceneProject(string file);


		CEditObject *FindObjectByPointer(void *p);
};

#endif
