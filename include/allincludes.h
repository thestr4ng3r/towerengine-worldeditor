#ifndef _WORLD_EDITOR_INCLUDES_H
#define _WORLD_EDITOR_INCLUDES_H

class CRenderWidget;
class CMainWindow;
class CPreview;

#include <towerengine.h>

#include "xmlutils.h"

#include "edittransform.h"
#include "property.h"
#include "editasset.h"
#include "singlefileeditasset.h"
#include "mesheditasset.h"
#include "cubemapeditasset.h"

#include "editobject.h"
#include "mesheditobject.h"
#include "directionallighteditobject.h"
#include "pointlighteditobject.h"

#include "editaxisobject.h"
#include "gridobject.h"
#include "editscene.h"
#include "sceneproject.h"
#include "worldeditor.h"
#include "editspace.h"

#include "vectoreditwidget.h"
#include "transformeditwidget.h"
#include "coloreditwidget.h"
#include "floateditwidget.h"
#include "stringeditwidget.h"
#include "asseteditwidget.h"
#include "fileeditwidget.h"
#include "viewsettingsdialog.h"

#include "outlineitemdata.h"
#include "selection.h"
#include "mainwindow.h"
#include "selectassetdialog.h"

using namespace std;

#endif
