#ifndef _EDITTRANSFORM_H
#define _EDITTRANSFORM_H

#include <towerengine.h>

class CEditTransform
{
	private:
		tVector position;
		tVector rotation;
		tVector scale;

	public:
		CEditTransform(tVector position, tVector rotation, tVector scale);
		CEditTransform(void);

		tVector GetPosition(void)			{ return position; }
		tVector GetRotation(void)			{ return rotation; }
		tVector GetScale(void)				{ return scale; }

		void SetPosition(tVector position)	{ this->position = position; }
		void SetRotation(tVector rotation)	{ this->rotation = rotation; }
		void SetScale(tVector scale)		{ this->scale = scale; }

		tTransform GetTransform(void);
};

#endif
