#ifndef _XMLUTILS_H
#define _XMLUTILS_H

#include <towerengine.h>

#include "edittransform.h"

#include <libxml/parser.h>

#define tcxc(a)	((const xmlChar *)a)
#define tcc(a)	((const char *)a)

xmlNodePtr CreateFloatXMLNode(const xmlChar *name, float v, const xmlChar *p = tcxc("v"));

xmlNodePtr CreateVectorXMLNode(const xmlChar *name, tVector v,
							   const xmlChar *x_p = tcxc("x"),
							   const xmlChar *y_p = tcxc("y"),
							   const xmlChar *z_p = tcxc("z"));

xmlNodePtr CreateEditTransformXMLNode(const xmlChar *name, CEditTransform trans);
xmlNodePtr CreateTransformXMLNode(const xmlChar *name, tTransform trans);

float ParseFloatXMLNode(xmlNodePtr node, const xmlChar *p = tcxc("v"));

tVector ParseVectorXMLNode(xmlNodePtr node,	const xmlChar *x_p = tcxc("x"),
											const xmlChar *y_p = tcxc("y"),
											const xmlChar *z_p = tcxc("z"));

CEditTransform ParseEditTransformXMLNode(xmlNodePtr node);



#endif
