#ifndef _SELECTASSETDIALOG_H
#define _SELECTASSETDIALOG_H

#include "ui_selectassetdialog.h"

class CAssetEditWidget;
class CMainWindow;

class CSelectAssetDialog : public QDialog, Ui::SelectAssetDialog
{
	Q_OBJECT

	private:
		CAssetEditWidget *edit_widget;
		CMainWindow *main_win;

		void UpdateAssetsList(void);

	public:
		CSelectAssetDialog(CAssetEditWidget *edit_widget, CMainWindow *main_win, QWidget *parent = 0);
		~CSelectAssetDialog();

	protected slots:
		void accept(void);
};

#endif
