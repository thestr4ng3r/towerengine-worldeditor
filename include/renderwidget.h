#ifndef _RENDERWIDGET_H
#define _RENDERWIDGET_H

#include <QGLWidget>

class CWorldEditor;

class CRenderWidget : public QGLWidget
{
	Q_OBJECT

	private:
		CWorldEditor *world_editor;
		int last_mouse_x, last_mouse_y;
		bool up_pressed, down_pressed, left_pressed, right_pressed, fast_pressed;

		bool camera_rotate_mode;
		int camera_rotate_delay;
		QPoint initial_mouse_pos;

	public:
		explicit CRenderWidget(CWorldEditor *world_editor, QWidget *parent = 0);

	protected:
		void initializeGL();
		//void paintGL(void);
		void resizeGL(int width, int height);
		void paintEvent(QPaintEvent *event);
		void mousePressEvent(QMouseEvent *event);
		void mouseReleaseEvent(QMouseEvent *event);
		void mouseMoveEvent(QMouseEvent *event);
		void keyPressEvent(QKeyEvent *event);
		void keyReleaseEvent(QKeyEvent *event);
		void timerEvent(QTimerEvent *event);
};

#endif
