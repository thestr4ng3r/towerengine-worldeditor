#ifndef _EDITSCENE_H
#define _EDITSCENE_H

class CSceneProject;
class CCubeMapEditAsset;
class tSkyBox;

#include "asseteditwidget.h"
#include "property.h"

class CEditScene : public CPropertiesContainer
{
	private:
		CSceneProject *project;

		CCubeMapEditAsset *cubemap;

	public:
		CEditScene(CSceneProject *project);
		~CEditScene(void);

		void SetSkyCubeMap(CCubeMapEditAsset *cubemap);

		CCubeMapEditAsset *GetSkyCubeMap(void)	{ return cubemap; }

		CSceneProject *GetSceneProject(void)	{ return project; }
};

class CEditSceneCubeMapAssetProperty : public CProperty, CAssetEditWidgetCallback
{
	private:
		CEditScene *scene;

	public:
		CEditSceneCubeMapAssetProperty(CEditScene *scene);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(CEditAsset *v);
};

#endif
