#ifndef _EDITASSET_H
#define _EDITASSET_H

#include <string.h>

#include "stringeditwidget.h"
#include "property.h"

#define EDIT_ASSET_TYPE_MESH		0
#define EDIT_ASSET_TYPE_CUBE_MAP	1

class CSceneProject;

class CEditAsset : public CPropertiesContainer
{
	private:
		CSceneProject *project;
		string name;

	public:
		CEditAsset(CSceneProject *project, string name);
		virtual ~CEditAsset(void)	{}

		void SetName(string name);

		string GetName(void)		{ return name; }
		virtual int GetType(void) =0;

		xmlNodePtr CreateSceneExportXMLNode(void);
		xmlNodePtr CreateSceneProjectXMLNode(void);

		virtual const xmlChar *GetSceneExportXMLNodeName(void) =0;
		virtual const xmlChar *GetSceneProjectXMLNodeName(void) =0;
};


class CEditAssetNameProperty : public CProperty, CStringEditWidgetCallback
{
	private:
		CEditAsset *asset;

	public:
		CEditAssetNameProperty(CEditAsset *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneExportXMLNode(void)	{ return 0; }
		xmlNodePtr CreateSceneProjectXMLNode(void)	{ return 0; }

		void ValueChanged(string s, string *c);
};


#endif
