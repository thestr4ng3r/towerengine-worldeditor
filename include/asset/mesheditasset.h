#ifndef _MESHEDITASSET_H
#define _MESHEDITASSET_H

#include <QByteArray>

class CEditAsset;
class CSingleFileEditAsset;

class CMeshEditAsset : public CSingleFileEditAsset
{
	private:
		tMesh *mesh;

		void FileChanged(void);

	public:
		CMeshEditAsset(CSceneProject *project, string name, string filename);
		~CMeshEditAsset(void);

		tMesh *GetMesh(void)		{ return mesh; }

		int GetType(void)			{ return EDIT_ASSET_TYPE_MESH; }

		QString GetFileFilter(void)	{ return QString("Tower Engine Mesh (*.tem)"); }

		const xmlChar *GetSceneProjectXMLNodeName(void)		{ return tcxc("mesh"); }
		const xmlChar *GetSceneExportXMLNodeName(void)		{ return tcxc("mesh"); }
};

#endif
