#ifndef _CUBEMAPEDITASSET_H
#define _CUBEMAPEDITASSET_H

#include "xmlutils.h"
#include "editasset.h"
#include "singlefileeditasset.h"

class CCubeMapEditAsset : public CSingleFileEditAsset
{
	private:
		GLuint cube_map;

		void FileChanged(void);

	public:
		CCubeMapEditAsset(CSceneProject *project, string name, string filename);
		~CCubeMapEditAsset(void);

		GLuint GetCubeMap(void)		{ return cube_map; }

		int GetType(void)			{ return EDIT_ASSET_TYPE_CUBE_MAP; }

		QString GetFileFilter(void)	{ return QString("Images (*.png *.jpg *.bmp *.tga);;Any File (*)"); }

		const xmlChar *GetSceneProjectXMLNodeName(void)		{ return tcxc("cubemap"); }
		const xmlChar *GetSceneExportXMLNodeName(void)		{ return tcxc("cubemap"); }
};

#endif
