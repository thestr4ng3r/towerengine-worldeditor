#ifndef _SINGLEFILEEDITASSET_H
#define _SINGLEFILEEDITASSET_H

#include <string>

#include "fileeditwidget.h"
#include "property.h"

class CSingleFileEditAsset : public CEditAsset
{
	private:
		string filename;
		QByteArray data;
		bool export_base64;

		void LoadFile(void);
		virtual void FileChanged(void) {}

	public:
		CSingleFileEditAsset(CSceneProject *project, string name, bool export_base64);
		CSingleFileEditAsset(CSceneProject *project, string name, string filename, bool export_base64);
		virtual ~CSingleFileEditAsset(void);

		void SetFileName(string f);
		string GetFileName(void)	{ return filename; }
		QByteArray GetData(void)	{ return data; }

		string GetFileExtension(void);
		bool GetExportBase64(void)	{ return export_base64; }

		virtual QString GetFileFilter(void)		{ return QString(); }

};

class CSingleFileEditAssetFilenameProperty : public CProperty, CFileEditWidgetCallback
{
	private:
		CSingleFileEditAsset *asset;

	public:
		CSingleFileEditAssetFilenameProperty(CSingleFileEditAsset *asset);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneExportXMLNode(void);
		xmlNodePtr CreateSceneProjectXMLNode(void);

		void ValueChanged(string v);
};

#endif
