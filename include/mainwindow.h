#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H

#include <QMainWindow>

#include "ui_mainwindow.h"

#include <string>
#include <list>

#include <towerengine.h>

class CWorldEditor;
class CRenderWidget;
class CEditObject;
class CSelection;
class CEditAsset;
class CRenderWidget;

#include "edittransform.h"

class CMainWindow : public QMainWindow, Ui::MainWindow
{
	Q_OBJECT

	public:
		explicit CMainWindow(CWorldEditor *world_editor, QWidget *parent = 0);
		~CMainWindow();

		void InitSceneProjectChange(void);
		void UpdateSceneInterface(void);
		void StartLoadingProject(void)	{ StatusBar->showMessage(tr("Loading Project...")); }
		void FinishLoadingProject(void)	{ StatusBar->clearMessage(); }

		void UpdateTitle(void);

		static void ClearLayout(QLayout *layout);
		static QFrame *CreateLine(QWidget *parent = 0);

		static QIcon *GetObjectTypeIcon(int type);
		static QIcon *GetAssetTypeIcon(int type);

		void InitialUpdate(void)	{ UpdateOutline(); UpdateAssetsList(); }


		void ObjectNameChanged(CEditObject *object);
		void ObjectTransformChanged(CEditObject *object);
		void AssetNameChanged(CEditAsset *asset);

		void SelectionChanged(void);

	private:
		CWorldEditor *world_editor;

		CRenderWidget *render_widget;

		void AddPropertiesWidget(QWidget *widget);
		void ClearPropertiesWidgets(void);

		void UpdateOutline(void);
		void UpdateAssetsList(void);

		void NewObject(CEditObject *object);
		void NewAsset(CEditAsset *asset);

		void ClearSelection(void);
		void InitObjectSelection(CEditObject *object);
		void InitAssetSelection(CEditAsset *asset);
		void SelectScene(void);

		void UpdateEditAxes(void);

	protected slots:
		void AddMeshObject(void);
		void AddDirectionalLight(void);
		void AddPointLight(void);
		void SelectOutlineItem(void);

		void SetDockWidgetsVisibilityFromActions(void);
		void SetDockWidgetsVisibilityActions(void);

		void DeleteSelection(void);

		void CreateMeshAsset(void);
		void CreateCubeMapAsset(void);
		void SelectAssetListItem(void);

		void InsertMeshToWorld(void);

		void Save(void);
		void SaveAs(void);

		void OpenProject(void);

		void ExportTowerEngineScene(void);
		void OpenViewSettings(void);
};

#endif
