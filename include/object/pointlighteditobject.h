#ifndef _POINTLIGHTEDITOBJECT_H
#define _POINTLIGHTEDITOBJECT_H

#include "vectoreditwidget.h"
#include "coloreditwidget.h"
#include "floateditwidget.h"
#include "property.h"

#include "editobject.h"

class CPointLightEditObject;

class CPointLightEditObjectSpaceConnection : public CEditObjectSpaceConnection
{
	private:
		CPointLightEditObject *object;

		tPointLight *light;

		btCollisionShape *collision_shape;
		btDefaultMotionState *motion_state;
		btRigidBody *rigid_body;

	public:
		CPointLightEditObjectSpaceConnection(CPointLightEditObject *object, CEditSpace *space);
		~CPointLightEditObjectSpaceConnection(void);

		void Update(void);
};


class CPointLightEditObject : public CEditObject
{
	private:
		CPointLightEditObjectSpaceConnection *space_connection;

		tVector position;
		tVector color;
		float distance;

		void UpdateLightProps(void);

	public:
		CPointLightEditObject(CSceneProject *project, tVector pos, tVector color, float dist);
		~CPointLightEditObject(void);

		int GetType(void)	{ return EDIT_OBJECT_TYPE_POINT_LIGHT; }

		void SetPosition(tVector position)	{ this->position = position; if(space_connection) space_connection->Update(); }
		void SetColor(tVector color)		{ this->color = color; if(space_connection) space_connection->Update(); }
		void SetDistance(float distance)	{ this->distance = distance; if(space_connection) space_connection->Update(); }

		tVector GetPosition(void)			{ return position; }
		tVector GetColor(void)				{ return color; }
		float GetDistance(void)				{ return distance; }

		const xmlChar *GetSceneProjectXMLNodeName(void)		{ return (const xmlChar *)"point_light"; }
		const xmlChar *GetSceneExportXMLNodeName(void)		{ return (const xmlChar *)"point_light"; }

		CPointLightEditObjectSpaceConnection *CreateSpaceConnection(CEditSpace *space);
};

class CPointLightEditObjectPositionProperty : public CProperty, CVectorEditWidgetCallback
{
	private:
		CPointLightEditObject *object;

	public:
		CPointLightEditObjectPositionProperty(CPointLightEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(tVector v);
};

class CPointLightEditObjectColorProperty : public CProperty, CColorEditWidgetCallback
{
	private:
		CPointLightEditObject *object;

	public:
		CPointLightEditObjectColorProperty(CPointLightEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(tVector v);
};

class CPointLightEditObjectDistanceProperty : public CProperty, CFloatEditWidgetCallback
{
	private:
		CPointLightEditObject *object;

	public:
		CPointLightEditObjectDistanceProperty(CPointLightEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(float v);
};


#endif
