#ifndef _DIRECTIONALLIGHTEDITOBJECT_H
#define _DIRECTIONALLIGHTEDITOBJECT_H

#include "vectoreditwidget.h"
#include "coloreditwidget.h"
#include "property.h"

#include "editobject.h"

class CDirectionalLightEditObject;

class CDirectionalLightEditObjectSpaceConnection : public CEditObjectSpaceConnection
{
	private:
		CDirectionalLightEditObject *object;

		tDirectionalLight *light;

	public:
		CDirectionalLightEditObjectSpaceConnection(CDirectionalLightEditObject *object, CEditSpace *space);
		~CDirectionalLightEditObjectSpaceConnection(void);

		void Update(void);
};


class CDirectionalLightEditObject : public CEditObject
{
	private:
		tVector direction;
		tVector color;

		CDirectionalLightEditObjectSpaceConnection *space_connection;

	public:
		CDirectionalLightEditObject(CSceneProject *project, tVector dir, tVector color);
		~CDirectionalLightEditObject(void);

		int GetType(void)		{ return EDIT_OBJECT_TYPE_DIRECTIONAL_LIGHT; }

		void SetDirection(tVector direction)	{ this->direction = direction; if(space_connection) space_connection->Update(); }
		void SetColor(tVector color)			{ this->color = color; if(space_connection) space_connection->Update(); }

		tVector GetDirection(void)			{ return direction; }
		tVector GetColor(void)				{ return color; }

		const xmlChar *GetSceneProjectXMLNodeName(void)		{ return tcxc("dir_light"); }
		const xmlChar *GetSceneExportXMLNodeName(void)		{ return tcxc("dir_light"); }

		CDirectionalLightEditObjectSpaceConnection *CreateSpaceConnection(CEditSpace *space);
};


class CDirectionalLightEditObjectDirectionProperty : public CProperty, CVectorEditWidgetCallback
{
	private:
		CDirectionalLightEditObject *object;

	public:
		CDirectionalLightEditObjectDirectionProperty(CDirectionalLightEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(tVector v);
};

class CDirectionalLightEditObjectColorProperty : public CProperty, CColorEditWidgetCallback
{
	private:
		CDirectionalLightEditObject *object;

	public:
		CDirectionalLightEditObjectColorProperty(CDirectionalLightEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(tVector v);
};



#endif
