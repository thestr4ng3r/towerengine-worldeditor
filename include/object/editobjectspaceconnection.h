#ifndef _EDITOBJECTSPACECONNECTION_H
#define _EDITOBJECTSPACECONNECTION_H

class CEditSpace;
class CEditObject;

class CEditObjectSpaceConnection
{
	private:
		CEditObject *object;
		CEditSpace *space;

	public:
		CEditObjectSpaceConnection(CEditObject *object, CEditSpace *space);
		virtual ~CEditObjectSpaceConnection(void) {}

		CEditObject *GetEditObject(void)	{ return object; }
		CEditSpace *GetEditSpace(void)		{ return space; }
};

#endif
