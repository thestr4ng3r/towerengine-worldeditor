#ifndef _EDITOBJECT_H
#define _EDITOBJECT_H

#include <QString>
#include <vector>

#include "stringeditwidget.h"
#include "property.h"
#include "editobjectspaceconnection.h"

#define EDIT_OBJECT_TYPE_MESH				0
#define EDIT_OBJECT_TYPE_DIRECTIONAL_LIGHT	1
#define EDIT_OBJECT_TYPE_POINT_LIGHT		2

class CSceneProject;

class CEditObject : public CPropertiesContainer
{
	protected:
		string name;

		CSceneProject *project;

	public:
		CEditObject(CSceneProject *project);
		virtual ~CEditObject(void)		{}

		void SetName(string name);

		string GetName(void)			{ return name; }
		virtual int GetType(void)		{ return 0; }

		CSceneProject *GetSceneProject(void)	{ return project; }

		xmlNodePtr CreateSceneExportXMLNode(void);
		xmlNodePtr CreateSceneProjectXMLNode(void);

		virtual const xmlChar *GetSceneExportXMLNodeName(void) =0;
		virtual const xmlChar *GetSceneProjectXMLNodeName(void) =0;

		//virtual bool GetOwnsWorldObject(void *p)	{ return false; }

		virtual void Move(tVector v) {}
		virtual void Scale(tVector v) {}
		virtual void Rotate(tVector v) {}

		virtual CEditObjectSpaceConnection *CreateSpaceConnection(CEditSpace *space) =0;
};

class CEditObjectNameProperty : public CProperty, CStringEditWidgetCallback
{
	private:
		CEditObject *object;

	public:
		CEditObjectNameProperty(CEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneExportXMLNode(void)	{ return 0; }
		xmlNodePtr CreateSceneProjectXMLNode(void)	{ return 0; }

		void ValueChanged(string s, string *c);
};


#endif
