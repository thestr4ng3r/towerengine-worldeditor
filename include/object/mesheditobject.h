#ifndef _MESHEDITOBJECT_H
#define _MESHEDITOBJECT_H

#include "transformeditwidget.h"
#include "asseteditwidget.h"
#include "property.h"

#include "editobject.h"
#include "editobjectspaceconnection.h"

class CMeshEditObject;

class CMeshEditObjectSpaceConnection : public CEditObjectSpaceConnection
{
	private:
		CMeshEditObject *object;

		tMeshObject *mesh_object;
		btCollisionObject *collision_object;

		btCollisionShape *collision_shape;
		btDefaultMotionState *motion_state;
		btRigidBody *rigid_body;

	public:
		CMeshEditObjectSpaceConnection(CMeshEditObject *object, CEditSpace *space);
		~CMeshEditObjectSpaceConnection(void);

		void UpdateMeshObject(void);
		void UpdateMeshObjectProps(void);

		tMeshObject *GetMeshObject(void)	{ return mesh_object; }
};



class CMeshEditObject : public CEditObject
{
	private:
		CMeshEditAsset *mesh_asset;
		CEditTransform transform;

		CMeshEditObjectSpaceConnection *space_connection;

		void UpdateObjectProps(void);

	public:
		CMeshEditObject(CSceneProject *project, CMeshEditAsset *mesh_asset = 0);
		~CMeshEditObject(void);

		int GetType(void)		{ return EDIT_OBJECT_TYPE_MESH; }

		void SetMeshAsset(CMeshEditAsset *mesh_asset);
		void SetTransform(CEditTransform t);

		CEditTransform GetTransform(void)	{ return transform; }
		CMeshEditAsset *GetMeshAsset(void)	{ return mesh_asset; }

		void MeshAssetContentChanged(void);

		const xmlChar *GetSceneProjectXMLNodeName(void)		{ return (const xmlChar *)"mesh"; }
		const xmlChar *GetSceneExportXMLNodeName(void)		{ return (const xmlChar *)"mesh"; }

		//bool GetOwnsWorldObject(void *p)	{ return p == (void *)object; }

		void Move(tVector v);

		CMeshEditObjectSpaceConnection *CreateSpaceConnection(CEditSpace *space);
};



class CMeshEditObjectTransformProperty : public CProperty, CTransformEditWidgetCallback
{
	private:
		CMeshEditObject *object;

	public:
		CMeshEditObjectTransformProperty(CMeshEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(CEditTransform t);
};

class CMeshEditObjectMeshAssetProperty : public CProperty, CAssetEditWidgetCallback
{
	private:
		CMeshEditObject *object;

	public:
		CMeshEditObjectMeshAssetProperty(CMeshEditObject *object);

		QWidget *CreateEditWidget(CMainWindow *main_win);
		xmlNodePtr CreateSceneProjectXMLNode(void);
		xmlNodePtr CreateSceneExportXMLNode(void);

		void ValueChanged(CEditAsset *v);
};

#endif
