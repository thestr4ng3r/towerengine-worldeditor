#ifndef _OBJECTSTREEITEMDATA_H
#define _OBJECTSTREEITEMDATA_H

class COutlineItemData
{
	public:
		enum Type { OBJECTS_TREE_ITEM_SCENE, OBJECTS_TREE_ITEM_OBJECT };

		COutlineItemData(Type type = OBJECTS_TREE_ITEM_OBJECT, void *object = 0)
		{
			this->type = type;
			this->object = object;
		}

		Type type;
		void *object;
};

Q_DECLARE_METATYPE(COutlineItemData)

#endif
