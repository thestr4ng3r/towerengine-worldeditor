#ifndef _EDITSPACE_H
#define _EDITSPACE_H

#include <towerengine.h>

#include "worldeditor.h"
#include "gridobject.h"
#include "editaxisobject.h"
#include "editobjectspaceconnection.h"

class CEditSpace
{
	private:
		CWorldEditor *world_editor;

		tWorld *world;
		tRenderer *renderer;
		int render_width, render_height;

		tVector cam_pos;
		tVector2 cam_rot;
		tVector cam_dir;

		float cam_speed_normal, cam_speed_fast;
		float cam_angle;

		struct physics
		{
			btBroadphaseInterface *broadphase;
			btDefaultCollisionConfiguration *collision_configuration;
			btCollisionDispatcher *collision_dispatcher;
			btSequentialImpulseConstraintSolver *solver;
			btDiscreteDynamicsWorld *world;
		} physics;

		tSkyBox *default_sky_box;
		tCoordinateSystemObject *coordinate_system_object;
		CGridObject *grid_object;

		CEditAxisObject *x_move_edit_axis;
		CEditAxisObject *y_move_edit_axis;
		CEditAxisObject *z_move_edit_axis;
		tVector edit_axes_pos;

		tSkyBox *scene_sky_box;

		map<CEditObject *, CEditObjectSpaceConnection *> object_connections;

		bool drag_moving;
		tVector2 drag_move_vec;
		int drag_move_axis;

	public:
		CEditSpace(CWorldEditor *world_editor);
		~CEditSpace(void);

		void SetScreenSize(int width, int height);

		void RotateCamera(float x, float y);
		void MoveCamera(float x, float z, bool fast);

		void SetEditCamera(tVector pos, tVector2 rot);

		void Init(int width = 0, int height = 0);
		void Render(void);

		void AddObject(CEditObject *object);
		void RemoveObject(CEditObject *object);
		void SetFromProject(CSceneProject *project);
		void Clear(void);
		void SetSkyBoxFromEditScene(CEditScene *scene);

		tVector GetEditCameraPosition(void)		{ return cam_pos; }
		tVector2 GetEditCameraRotation(void)	{ return cam_rot; }

		tSkyBox *GetDefaultSkyBox(void)	{ return default_sky_box; }
		tWorld *GetWorld(void)	{ return world; }
		btDiscreteDynamicsWorld *GetPhysicsWorld(void)	{ return physics.world; }

		void MouseDown(float x, float y);
		void MouseDrag(float dx, float dy);
		void MouseUp(void);

		void SetMoveEditAxes(bool visible, tVector pos);
		void SetViewSettings(float cam_speed_normal, float cam_speed_fast, float cam_angle);

		float GetCamSpeedNormal(void)	{ return cam_speed_normal; }
		float GetCamSpeedFast(void)		{ return cam_speed_fast; }
		float GetCamAngle(void)			{ return cam_angle; }


		int FindEditAxisByPointer(void *p);
		CEditObject *FindObjectByPointer(void *p);
};


#endif
