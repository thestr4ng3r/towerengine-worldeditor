#ifndef _TOWERSTYLE_H
#define _TOWERSTYLE_H

#include <QProxyStyle>

class CTowerStyle : public QProxyStyle
{
	Q_OBJECT

	public:
		CTowerStyle(QStyle *parent = 0);

		static QPainterPath roundRectPath(const QRect &rect);

		int pixelMetric(PixelMetric metric, const QStyleOption *option, const QWidget *widget) const;
		void drawPrimitive(PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const;

};

#endif
