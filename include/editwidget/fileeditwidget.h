#ifndef _FILEEDITWIDGET_H
#define _FILEEDITWIDGET_H

#include <QWidget>
#include <string>

#include "ui_fileeditwidget.h"

#include <towerengine.h>

class CFileEditWidgetCallback
{
	public:
		virtual void ValueChanged(string v) =0;
};

class CFileEditWidget : public QWidget, Ui::FileEditWidget
{
	Q_OBJECT

	private:
		QWidget *main_win;

		QString name_filter;

		CFileEditWidgetCallback *callback;

	public:
		CFileEditWidget(QString name, string filename, QString name_filter, QWidget *main_win = 0, QWidget *parent = 0);
		~CFileEditWidget();

		string GetValue(void);
		void SetValue(string v);

		void SetCallback(CFileEditWidgetCallback *callback)	{ this->callback = callback; }

	protected slots:
		void OpenFile(void);
		void EditFinished(void);

	signals:
		void ValueChanged(string v);
};

#endif
