#ifndef ASSETEDITWIDGET_H
#define ASSETEDITWIDGET_H

#include <QWidget>

#include "ui_asseteditwidget.h"

class CSceneProject;
class CEditAsset;
class CMainWindow;

class CAssetEditWidgetCallback
{
	public:
		virtual void ValueChanged(CEditAsset *asset) =0;
};

class CAssetEditWidget : public QWidget, Ui::AssetEditWidget
{
	Q_OBJECT

	private:
		CSceneProject *project;
		CEditAsset *asset;
		int type;

		CMainWindow *main_win;

		CAssetEditWidgetCallback *callback;

	public:
		CAssetEditWidget(QString name, CSceneProject *project, int asset_type, CMainWindow *main_win, CEditAsset *asset = 0, QWidget *parent = 0);
		~CAssetEditWidget();

		void SetValue(CEditAsset *asset);
		void ChangeValue(CEditAsset *asset); // sets value and emits ValueChanged()
		CEditAsset *GetValue(void);
		int GetType(void)					{ return type; }
		CSceneProject *GetProject(void)		{ return project; }

		void SetCallback(CAssetEditWidgetCallback *callback)	{ this->callback = callback; }

	signals:
		void ValueChanged(CEditAsset *asset);

	protected slots:
		void SelectAsset(void);
};

#endif
