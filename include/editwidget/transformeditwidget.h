#ifndef TRANSFORMEDITWIDGET_H
#define TRANSFORMEDITWIDGET_H

#include <QWidget>

#include "ui_transformeditwidget.h"

#include "edittransform.h"

class CTransformEditWidgetCallback
{
	public:
		virtual void ValueChanged(CEditTransform t) =0;
};

// TODO: remove QObject, signal stuff
class CTransformEditWidget : public QWidget, Ui::TransformEditWidget
{
	Q_OBJECT

	private:
		CTransformEditWidgetCallback *callback;

		void TriggerValueChanged(CEditTransform t);

	public:
		CTransformEditWidget(CEditTransform t, QWidget *parent = 0);
		~CTransformEditWidget();

		void SetValue(CEditTransform t);
		CEditTransform GetValue(void);

		void SetCallback(CTransformEditWidgetCallback *callback)	{ this->callback = callback; }

	signals:
		void ValueChanged(CEditTransform t);

	protected slots:
		void PositionXEditFinished();
		void PositionYEditFinished();
		void PositionZEditFinished();

		void RotationXEditFinished();
		void RotationYEditFinished();
		void RotationZEditFinished();

		void ScaleXEditFinished();
		void ScaleYEditFinished();
		void ScaleZEditFinished();
};

#endif
