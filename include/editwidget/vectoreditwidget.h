#ifndef _VECTOREDITWIDGET_H
#define _VECTOREDITWIDGET_H

#include <QWidget>

#include "ui_vectoreditwidget.h"

#include <towerengine.h>


class CVectorEditWidgetCallback
{
	public:
		virtual void ValueChanged(tVector v) =0;
};


class CVectorEditWidget : public QWidget, Ui::VectorEditWidget
{
	Q_OBJECT

	private:
		CVectorEditWidgetCallback *callback;

		void TriggerValueChanged(tVector v);

	public:
		CVectorEditWidget(QString name, tVector v = Vec(0.0, 0.0, 0.0), QWidget *parent = 0);
		~CVectorEditWidget();

		void SetValue(tVector v);
		tVector GetValue(void);

		void SetCallback(CVectorEditWidgetCallback *callback)	{ this->callback = callback; }

	signals:
		void ValueChanged(tVector v);

	protected slots:
		void XEditFinished(void);
		void YEditFinished(void);
		void ZEditFinished(void);
};

#endif
