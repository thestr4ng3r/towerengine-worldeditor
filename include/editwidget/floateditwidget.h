#ifndef _FLOATEDITWIDGET_H
#define _FLOATEDITWIDGET_H

#include <QWidget>

#include "ui_floateditwidget.h"

#include <towerengine.h>

class CFloatEditWidgetCallback
{
	public:
		virtual void ValueChanged(float f) =0;
};

class CFloatEditWidget : public QWidget, Ui::FloatEditWidget
{
	Q_OBJECT

	private:
		CFloatEditWidgetCallback *callback;

	public:
		CFloatEditWidget(QString name, float v, float min, float max, QWidget *parent = 0);
		~CFloatEditWidget();

		void SetValue(float v);
		float GetValue(void);

		void SetCallback(CFloatEditWidgetCallback *callback)	{ this->callback = callback; }

	signals:
		void ValueChanged(float v);

	protected slots:
		void EditFinished(void);
};

#endif
