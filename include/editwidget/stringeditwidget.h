#ifndef _STRINGEDITWIDGET_H
#define _STRINGEDITWIDGET_H

#include <QWidget>

#include "ui_stringeditwidget.h"

#include <towerengine.h>


class CStringEditWidgetCallback
{
	public:
		virtual void ValueChanged(string v, string *c) =0;
};

class CStringEditWidget : public QWidget, Ui::StringEditWidget
{
	Q_OBJECT

	private:
		CStringEditWidgetCallback *callback;

	public:
		CStringEditWidget(QString name, string v, QWidget *parent = 0);
		~CStringEditWidget();

		void SetValue(string v);
		string GetValue(void);

		void SetCallback(CStringEditWidgetCallback *callback)	{ this->callback = callback; }

	signals:
		void ValueChanged(string v, string *c);

	protected slots:
		void EditFinished(void);
};

#endif
