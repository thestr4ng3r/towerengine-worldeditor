#ifndef _COLOREDITWIDGET_H
#define _COLOREDITWIDGET_H

#include <QWidget>

#include "ui_coloreditwidget.h"

#include <towerengine.h>

class CColorEditWidgetCallback
{
	public:
		virtual void ValueChanged(tVector v) =0;
};

class CColorEditWidget : public QWidget, Ui::ColorEditWidget
{
	Q_OBJECT

	private:
		CColorEditWidgetCallback *callback;

		void TriggerValueChanged(tVector v);

	public:
		CColorEditWidget(QString name, tVector v = Vec(0.0, 0.0, 0.0), QWidget *parent = 0);
		~CColorEditWidget();

		void SetValue(tVector v);
		tVector GetValue(void);

		void SetCallback(CColorEditWidgetCallback *callback)	{ this->callback = callback; }

	signals:
		void ValueChanged(tVector v);

	protected slots:
		void REditFinished(void);
		void GEditFinished(void);
		void BEditFinished(void);
};

#endif
