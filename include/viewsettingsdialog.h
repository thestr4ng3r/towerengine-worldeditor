#ifndef _VIEWSETTINGSDIALOG_H
#define _VIEWSETTINGSDIALOG_H

#include <QDialog>

#include "ui_viewsettingsdialog.h"

class CMainWindow;
class CSceneProject;
class CEditSpace;

class CViewSettingsDialog : public QDialog, Ui::ViewSettingsDialog
{
	Q_OBJECT

	private:
		CMainWindow *main_win;
		CEditSpace *edit_space;

	public:
		CViewSettingsDialog(CMainWindow *parent, CEditSpace *edit_space);
		~CViewSettingsDialog();

	protected slots:
		void accept(void);

		void UpdateCameraAngleLabel(void);
};

#endif
